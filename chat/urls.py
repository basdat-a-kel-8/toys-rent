from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'chat'
urlpatterns = [
    path('chatanggota', chatAnggota, name='chatAnggota'),
    path('chatadmin', chatAdmin, name='chatAdmin'),
]