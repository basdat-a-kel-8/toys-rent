from django.shortcuts import render
from user.models import *
from django.db import connection
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse
from .forms import ChatForm
import random
import calendar
import time

# Create your views here.
def chatAnggota(request):
    if request.method=='POST':
        form = ChatForm(request.POST)
        if form.is_valid():
            message = form.cleaned_data.get("message")
            print(message)
            id_chat = random.randint(51, 1000)
            id_chat_exists = list(Chat.objects.raw("select * from chat where id=%s", [str(id_chat)]))
            while(id_chat_exists):
                id_chat = random.randint(51, 1000)
                id_chat_exists = list(Chat.objects.raw("select * from chat where id=%s", [str(id_chat)]))
            time_stamp = time.ctime(calendar.timegm(time.gmtime()))
            with connection.cursor() as c:
                c.execute("select * from admin")
                admin_query = c.fetchall()
            tebak_admin_berhadiah = [i[0] for i in admin_query]
            print(tebak_admin_berhadiah)
            ktp_admin = random.choice(tebak_admin_berhadiah)
            data = [str(id_chat), message, time_stamp, request.session['no_ktp'], ktp_admin]
            with connection.cursor() as c:
                c.execute("insert into chat values(%s,%s,%s,%s,%s)", data)
            data = {'pesan': message,'datetime':time_stamp}
            return JsonResponse(data)
    else:
        no_ktp_anggota = request.session['no_ktp']
        with connection.cursor() as c:
            c.execute("select * from chat where no_ktp_anggota = %s", [no_ktp_anggota])
            chat_data = c.fetchall()
        response = {}
        data = [{'pesan': i[1],
            'datetime': i[2],
            'ktp_admin':i[3]} for i in chat_data]
        response['data'] = data
        response['forms'] = ChatForm()
        return render(request, 'chat/chatanggota.html', response)

def chatAdmin(request):
    return render(request, 'chat/chatadmin.html')