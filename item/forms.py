from django import forms


from django.core.exceptions import ValidationError

nama_attrs = {
        'type': 'text',
        'class': 'form-control form-control-pink',
        'placeholder':'Nama Lengkap',
}

kategori_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Kode Pos',
}

namaitem_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Nama Item',
}

deskripsi_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Deskripsi',
}
bahan_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Bahan',
}
usiadari_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Dari',
}

usiasampai_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Sampai',
}

class ItemForm(forms.Form):
    MOTORIK = 'Mainan Motorik'
    BERWARNA = 'Mainan Berwarna'
    RUMAH = 'Mainan Rumah'
    BESAR = 'Mainan Besar'
    PANTAI = 'Mainan Pantai'
    KATEGORI_CHOICES = (
        (MOTORIK, 'Mainan Motorik'),
        (BERWARNA, 'Mainan Berwarna'),
        (RUMAH, 'Mainan Rumah'),
        (BESAR, 'Mainan Besar'),
        (PANTAI, 'Mainan Pantai')
    )
    nama = forms.CharField(max_length=255, widget=forms.TextInput(attrs=namaitem_attrs))
    deskripsi = forms.CharField(max_length=1000, widget=forms.TextInput(attrs=deskripsi_attrs))
    usia_dari = forms.IntegerField(widget=forms.NumberInput(attrs=usiadari_attrs))
    usia_sampai = forms.IntegerField(widget=forms.NumberInput(attrs=usiasampai_attrs))
    bahan = forms.CharField(max_length=1000, widget = forms.TextInput(attrs=bahan_attrs))
    kategori = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=kategori_attrs), choices=KATEGORI_CHOICES)