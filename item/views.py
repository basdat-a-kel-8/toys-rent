from django.shortcuts import render
from .forms import ItemForm
from user.models import Item, KategoriItem
from django.db import connection
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.
# dibantuin michael wiryadinata halim hehehe
def addItem(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            create_item(form)
            return HttpResponseRedirect('/item/list/1')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = ItemForm()
        return render (request, 'item/item.html', {'forms':forms})

def updateItem(request, nama):
    with connection.cursor() as c:
        c.execute("select * from item where nama = %s", [nama])
        item_query = c.fetchall()
        print(item_query)
        c.execute ("select nama_kategori from kategori_item where nama_item = %s", [nama])
        kategori_query = c.fetchall()
    
    kategori = [i[0] for i in kategori_query]
    print(kategori)
    item = {'nama': item_query[0][0], 'deskripsi': item_query[0][1], 'usia_dari': item_query[0][2],'usia_sampai': item_query[0][3],'kategori': kategori}
    print(item)
    if request.method == 'POST':
        form = ItemForm(request.POST, initial=item)
        if form.is_valid() and form.has_changed():
            with connection.cursor() as c:
                nama_item = item['nama']
                if 'nama' in form.changed_data:
                    create_item(form)
                    with connection.cursor() as c:
                        c.execute("DELETE FROM item WHERE nama = %s", [item])
                else:
                    for updated_column in form.changed_data:
                        if updated_column != 'kategori':
                            updated_data = form.cleaned_data.get(updated_column)
                            c.execute('update item set %s = %s where nama = %s' % (updated_column,'%s','%s'), [updated_data, nama])
                        else:
                            kategori_sama = intersection(kategori, form.cleaned_data.get('kategori'))
                            kategori_lama = (list(set(kategori) - set(kategori_sama)))
                            kategori_baru = (list(set(kategori_sama) - set(kategori)))
                            for nama_kategori_lama in kategori_lama:
                                c.execute("delete from kategori_item where nama_kategori = %s", [nama_kategori_lama])
                            for nama_kategori_baru in kategori_baru:
                                c.execute("insert into kategori_item values(%s,%s)"[nama_item, nama_kategori_baru])
            return HttpResponseRedirect('/item/list/1')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = ItemForm(item) 
        return render(request, 'item/updateitem.html', {'forms': form, 'nama_item': nama})

def listItem(request, page):
    with connection.cursor() as c:
        c.execute("SELECT * FROM KATEGORI_ITEM")
        item_list = c.fetchall()
        all_items = [{
            'nama_item':i[0],
            'nama_kategori':i[1],
        } for i in item_list]
    paginator = Paginator(all_items, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request, 'item/listitem.html', {'data': data})

def listOrdered(request, order, ini, page):
    with connection.cursor() as c:
        order_q = order
        ini_q = ini
        query = "SELECT * FROM KATEGORI_ITEM order by " + order + " " + ini
        c.execute(query)
        item_list = c.fetchall()
        all_items = [{
            'nama_item':i[0],
            'nama_kategori':i[1],
        } for i in item_list]
    paginator = Paginator(all_items, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request, 'item/listitem.html', {'data': data})

def kategoriFiltering(request, kategori, page):
    with connection.cursor() as c:
        c.execute("SELECT * FROM KATEGORI_ITEM where nama_kategori = %s",[kategori])
        item_list = c.fetchall()
        all_items = [{
            'nama_item':i[0],
            'nama_kategori':i[1],
        } for i in item_list]
    paginator = Paginator(all_items, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request, 'item/listitem.html', {'data': data})

def deleteItem(request, id):
    with connection.cursor() as c:
        c.execute("DELETE FROM item WHERE nama = %s", [id])
        return HttpResponseRedirect("/item/list/1")

def create_item(form):
    data_item = [form.cleaned_data.get('nama'), form.cleaned_data.get('deskripsi'), form.cleaned_data.get('usia_dari'), 
                            form.cleaned_data.get('usia_sampai'), form.cleaned_data.get('bahan')]
    with connection.cursor() as c:
        c.execute("INSERT INTO ITEM VALUES(%s, %s, %s, %s, %s)", data_item)
        for kategori in form.cleaned_data.get("kategori"):
            c.execute("INSERT INTO KATEGORI_ITEM VALUES(%s,%s)", [form.cleaned_data.get('nama'), kategori])

def intersection(lst1, lst2):
    # Use of hybrid method
    temp = set(lst2)
    lst3 = [value for value in lst1 if value in temp]
    return lst3