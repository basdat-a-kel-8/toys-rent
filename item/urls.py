from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'item'
urlpatterns = [
    path('add', addItem, name='addItem'),
    path('update/<str:nama>', updateItem, name="updateItem"),
    path('list/<str:page>', listItem, name='listItem'),
    path('delete/<str:id>', deleteItem, name='deleteItem'),
    path('list/<str:order>/<str:ini>/<str:page>', listOrdered, name="sorting"),
    path('list/<str:kategori>/<str:page>', kategoriFiltering, name="filtering")
]