from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'user'
urlpatterns = [
    path('anggota/signup', anggotaRegister, name='anggotaRegister'),
    path('admin/signup', adminRegister, name="adminRegister"),
    path('logout', logout, name="logout"),
    path('me', profile, name="profile"),
    path('login', login, name="login")
]