from django import forms


from django.core.exceptions import ValidationError

noktp_attrs = {
        'type': 'text',
        'class': 'form-control form-control-pink',
        'placeholder':'Nomor KTP',
    }




nama_attrs = {
        'type': 'text',
        'class': 'form-control form-control-pink',
        'placeholder':'Nama Lengkap',
}

email_attrs = {
    'type': 'text',
    'class': 'form-control form-control-pink',
    'placeholder':'Email',
}

tanggal_attrs = {
    'type': 'date',
    'class': 'form-control form-control-pink',
}

notelp_attrs = {
    'class': 'form-control form-control-pink',
    'placeholder':'No Telp',
}

alamat_attrs = {
    'type': 'text',
    'class': 'form-control form-control-pink',
    'placeholder':'Nama Alamat',
}

jalan_attrs = {
    'type': 'text',
    'class': 'form-control form-control-pink',
    'placeholder':'Nama jalan',
}

nomor_attrs = {
    'class': 'form-control form-control-pink',
    'placeholder':'Nomor',
}
kota_attrs = {
    'class': 'form-control form-control-pink',
    'placeholder':'Kota',
}
kodepos_attrs = {
    'class': 'form-control form-control-pink',
    'placeholder':'Kode Pos',
}

class AnggotaRegistrationForm(forms.Form):
    no_ktp = forms.CharField(max_length=200, widget=forms.TextInput(attrs=noktp_attrs))
    nama_lengkap = forms.CharField(max_length = 255, widget=forms.TextInput(attrs=nama_attrs))
    email = forms.EmailField(widget=forms.EmailInput(attrs=email_attrs))
    tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs=tanggal_attrs), required=False)
    no_telp = forms.IntegerField( widget=forms.NumberInput(attrs=notelp_attrs))
    nama_alamat = forms.CharField(max_length= 255, widget=forms.TextInput(attrs=alamat_attrs))
    jalan = forms.CharField(max_length= 255, widget=forms.TextInput(attrs=jalan_attrs))
    nomor = forms.CharField(max_length= 255, widget=forms.TextInput(attrs=nomor_attrs))
    kota = forms.CharField(max_length= 255, widget=forms.TextInput(attrs=kota_attrs))
    kodepos = forms.CharField(max_length= 255, widget=forms.TextInput(attrs=kodepos_attrs))

    
class AdminRegistrationForm(forms.Form):
    no_ktp = forms.CharField(max_length=20, widget=forms.TextInput(attrs=noktp_attrs))
    nama_lengkap = forms.CharField(max_length = 255, widget=forms.TextInput(attrs=nama_attrs))
    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
    tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs=tanggal_attrs))
    no_telp = forms.CharField(max_length=20, widget=forms.TextInput(attrs=notelp_attrs))

class CustomLoginForm(forms.Form):
    no_ktp = forms.CharField(max_length=20, widget=forms.TextInput(attrs=noktp_attrs))
    email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))