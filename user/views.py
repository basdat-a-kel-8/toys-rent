from django.shortcuts import render
from .forms import AnggotaRegistrationForm, CustomLoginForm, AdminRegistrationForm
from django.contrib import messages
from django.urls import reverse
from django.db import connection
from django.http import JsonResponse, HttpResponseRedirect
from .models import *
# Create your views here.

def login(request):
    if (request.method == 'POST'):
        form = CustomLoginForm(request.POST)
        if form.is_valid():
            no_ktp = form.cleaned_data.get('no_ktp')
            email = form.cleaned_data.get('email')
            user_exists = Pengguna.objects.raw('SELECT * FROM PENGGUNA WHERE no_ktp = %s and email = %s', [no_ktp, email])
            
            if list(user_exists):
                userisAnggota = Anggota.objects.raw('SELECT * FROM ANGGOTA WHERE no_ktp = %s', [no_ktp])
                if not list(userisAnggota):
                    print('masuk sini')
                    role = 'admin'
                else:
                    
                    role = 'anggota'
                data = {'no_ktp': no_ktp, 'email': email, 'role': role}
                request.session['no_ktp'] = no_ktp
                request.session['role'] = role
                print(request.session)
                return JsonResponse(data)  
            else:
                messages.error(request, 'Invalid email or KTP number')
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = CustomLoginForm()
        return render(request, 'user/login.html', {'forms': forms})

def anggotaRegister(request):
    if request.method == 'POST':
        form = AnggotaRegistrationForm(request.POST)
        
        if form.is_valid():
            new_user = check_anggota(form)
            if new_user:
                print('masuk sini')
                return HttpResponseRedirect(reverse('user:login'))
            else:
                print("line 45")
                messages.error(request, "Error occured, Your Email or KTP Number is already on database")
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            print(form.errors)
            messages.error(request, "Error occured, have you checked your data?")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = AnggotaRegistrationForm()
        return render(request, 'user/registration.html', {'forms':forms})

def adminRegister(request):
    if request.method == 'POST':
        form = AdminRegistrationForm(request.POST)
        if form.is_valid():
            new_admin = check_admin(form)
            if new_admin:
                return HttpResponseRedirect(reverse('user:login'))
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = AdminRegistrationForm()
        return render(request, 'user/adminregistration.html', {'forms': forms})

def profile(request):
    if (request.session['no_ktp']):
        no_ktp = request.session['no_ktp']
        with connection.cursor() as c:
            c.execute("select * from pengguna where no_ktp = %s", [no_ktp])
            pengguna_query = c.fetchall()
            if request.session['role'] == "anggota":
                c.execute("select * from anggota where no_ktp = %s", [no_ktp])
                anggota_query = c.fetchall()
                c.execute("select * from alamat where no_ktp_anggota = %s", [no_ktp])
                alamat_query = c.fetchall()
                alamat = [{'nama':i[1], 'jalan':i[2], 'nomor': i[3], 'kota': i[4], 'kodepos':i[5]}for i in alamat_query]
                data = {'no_ktp':pengguna_query[0][0], 'nama_lengkap': pengguna_query[0][1], 'email': pengguna_query[0][2], 'tanggal_lahir': pengguna_query[0][3], 'no_telp': pengguna_query[0][4]
                , 'poin': anggota_query[0][1], 'level': anggota_query[0][2], 'alamat':alamat}
            else:
                data =  {'no_ktp':pengguna_query[0][0], 'nama_lengkap': pengguna_query[0][1], 'email': pengguna_query[0][2], 'tanggal_lahir': pengguna_query[0][3], 'no_telp': pengguna_query[0][4]}
                print(data)
        return render(request, 'user/me.html',{'data': data})
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('user:login'))

def check_anggota(form):
    data_anggota = [form.cleaned_data.get('no_ktp'), form.cleaned_data.get('nama_lengkap'), form.cleaned_data.get('email'), form.cleaned_data.get('tanggal_lahir'), form.cleaned_data.get('no_telp')]
    alamat_anggota = [form.cleaned_data.get('no_ktp'), form.cleaned_data.get('nama_alamat'), form.cleaned_data.get('jalan'),form.cleaned_data.get('nomor'), form.cleaned_data.get('kota'), form.cleaned_data.get('kodepos')]    
    ktp_exists = Anggota.objects.raw("SELECT * FROM ANGGOTA WHERE no_ktp = %s", [form.cleaned_data.get('no_ktp')])
    if list(ktp_exists):
        print("line 47")
        return False
    else:
        email_exists = Pengguna.objects.raw("SELECT * FROM PENGGUNA WHERE email= %s", [form.cleaned_data.get('email')])
        if not list(email_exists):
            with connection.cursor() as c:
                c.execute("INSERT INTO PENGGUNA VALUES(%s, %s, %s, %s, %s)", data_anggota)
                c.execute("INSERT INTO ANGGOTA VALUES(%s, 0, 'BRONZE')",[form.cleaned_data.get('no_ktp')])
                c.execute("INSERT INTO ALAMAT VALUES(%s,%s,%s,%s,%s,%s)",alamat_anggota)
            return True
        else:
            print("line 84")
            return False

def check_admin(form):
    data_admin = [form.cleaned_data.get('no_ktp'), form.cleaned_data.get('nama_lengkap'), form.cleaned_data.get('email'), form.cleaned_data.get('tanggal_lahir'), form.cleaned_data.get('no_telp')]
    ktp_exists = Anggota.objects.raw("SELECT * FROM ANGGOTA WHERE no_ktp = %s", [form.cleaned_data.get('no_ktp')])
    if list(ktp_exists):
        return False 
    else:
        email_exists = Pengguna.objects.raw("SELECT * FROM PENGGUNA WHERE email= %s", [form.cleaned_data.get('email')])
        if not list(email_exists):
            with connection.cursor() as c:
                c.execute("INSERT INTO PENGGUNA VALUES(%s, %s, %s, %s, %s)", data_admin)
                c.execute("INSERT INTO ADMIN VALUES(%s)",[form.cleaned_data.get('no_ktp')])
            return True
        else:
            return False   