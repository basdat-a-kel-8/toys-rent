from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'pengiriman'
urlpatterns = [
    path('create', createPengiriman, name='createPengiriman'),
    path('update/<str:id>', updatePengiriman, name='updatePengiriman'),
    path('list/<str:page>', listPengiriman, name='listPengiriman'),
    path('delete/<str:id>', deletePengiriman , name='deletePengiriman')
]