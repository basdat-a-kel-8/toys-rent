import random
import string

''' ini tinggal import aja,
(ex: from generate_resi import generate_resi
jangan lupa di cek kalo hasil random
resinya ternyata udah ada di db
ex:
resi = generate_resi()
resi_exists = list(Pengiriman.objects.raw("select * from pengiriman where no_resi = %s", [resi]))
while resi_exists:
    resi = generate_resi()
    resi_exists = list(Pengiriman.objects.raw("select * from pengiriman where no_resi = %s", [resi]))'''    
def generate_resi():
    head = ''.join(random.choice(string.ascii_uppercase) for i in range(3))
    tail = str(random.randint(1000000,9999999))
    return head + tail
