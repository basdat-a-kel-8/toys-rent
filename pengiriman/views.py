from django.shortcuts import render
from .forms import PengirimanForm
from user.models import *
from django.db import connection
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import string
import random
# Create your views here.
def createPengiriman(request):
    if request.method == "POST":
        form = PengirimanForm(request.session['no_ktp'], request.POST )
        if form.is_valid():
            buat_pengiriman(form, request.session['no_ktp'])
            return HttpResponseRedirect('/pengiriman/list/1')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = PengirimanForm(no_ktp=request.session['no_ktp'])
        # forms = PengirimanForm(role=request.session['role'],no_ktp=request.session['no_ktp'])
        return render(request, 'pengiriman/createPengiriman.html', {'forms':forms})

def updatePengiriman(request,id):
    with connection.cursor() as c:
        c.execute("select * from pengiriman where no_resi=%s", [id])
        pengiriman = c.fetchall()
        print(pengiriman)
        c.execute
    forms = PengirimanForm(no_ktp=request.session['no_ktp'])
    # forms = PengirimanForm(role=request.session['role'],no_ktp=request.session['no_ktp'])
    return render(request, 'pengiriman/updatePengiriman.html', {'forms':forms})

def listPengiriman(request, page):
    no_ktp = request.session['no_ktp']
    role = request.session['role']
    with connection.cursor() as cursor:
        if role == 'admin':
            cursor.execute(
                'SELECT p.no_resi, tanggal, b.id_barang, p.id_pemesanan, (m.harga_sewa+p.ongkos) as total FROM PENGIRIMAN p, BARANG_PESANAN b , PEMESANAN m WHERE p.id_pemesanan = b.id_pemesanan and p.id_pemesanan = m.id_pemesanan'
            )
        else :
            cursor.execute(
                'SELECT p.no_resi, tanggal, b.id_barang, p.id_pemesanan, (m.harga_sewa+p.ongkos) as total FROM PENGIRIMAN p, BARANG_PESANAN b , PEMESANAN m WHERE p.id_pemesanan = b.id_pemesanan AND no_ktp_pemesan = %s and p.id_pemesanan=m.id_pemesanan',[no_ktp]
            )
        list_pengiriman = cursor.fetchall()
    data_pengiriman = [{'id_pengiriman':i[0],
        'tanggal' : i[1],
        'daftar_barang': i[2],
        'id_pemesanan': i[3],
        'harga_total': i[4],
    } for i in list_pengiriman]
    paginator = Paginator(data_pengiriman, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return render(request, 'pengiriman/listPengiriman.html', {'daftar_pengiriman':data})

def deletePengiriman(request,id):
    with connection.cursor() as c:
        c.execute("DELETE FROM PENGIRIMAN WHERE no_resi = %s", [id])
        return HttpResponseRedirect("/pengiriman/list/1")

def buat_pengiriman(form, ktp):
    alpha_numeric = string.ascii_uppercase + string.digits
    no_resi = ''.join(random.choice(alpha_numeric))
    data_pengiriman = [form.cleaned_data.get('barang_pesanan'),form.cleaned_data.get('alamat'),form.cleaned_data.get('tanggal'),form.cleaned_data.get('metode')]
    print(data_pengiriman)
    id_barang = data_pengiriman[0] 
    with connection.cursor() as c:
        c.execute("select b.id_pemesanan, b.id_barang, ongkos from barang_pesanan b, pemesanan p where no_ktp_pemesan = %s and p.status = 'SEDANG DISIAPKAN' and p.id_pemesanan=b.id_pemesanan", [ktp])
        data=c.fetchall()
        if(not data):
            return HttpResponseRedirect("/pengiriman/list/1")
        print(data)
        ongkos=data[0][2]
        id_pemesanan=data[0][0]
        for i in range(len(data)):
            if data_pengiriman[0][0] == id_barang:
                id_pemesanan = data[i][0]
                ongkos = data[i][2]
            c.execute("INSERT INTO PENGIRIMAN VALUES(%s, %s, %s, %s, %s, %s, %s)", [no_resi, id_pemesanan, data_pengiriman[3], ongkos, data_pengiriman[2], ktp, data_pengiriman[1][0]])
