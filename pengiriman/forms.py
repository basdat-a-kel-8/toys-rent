from django import forms
from user.models import Pengiriman
from django.core.exceptions import ValidationError
from django.db import connection

tanggal_attrs = {
    'type': 'date',
    'class': 'form-control form-border-pink',
}

metode_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Metode Pengiriman',
}

multiselect_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
}

review_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Review Barang',
}

def barang_choices(no_ktp):
    with connection.cursor() as c:
        c.execute("select id_barang from barang_pesanan b, pemesanan p where no_ktp_pemesan = %s and p.id_pemesanan = b.id_pemesanan", [no_ktp])
        ids = c.fetchall()
        result = [(i[0], i[0]) for i in ids]
        return result


def alamat_choices(no_ktp):
    with connection.cursor() as c:
        c.execute("select a.nama from alamat a, pengiriman p where a.no_ktp_anggota = %s and a.no_ktp_anggota = p.no_ktp_anggota", [no_ktp])
        alamat = c.fetchall()
        result = [(i[0], i[0]) for i in alamat]
        return result


class PengirimanForm(forms.Form):
    # BARANG1 = '1'
    # BARANG2 = '2'
    # BARANG3 = '3'
    # BARANG4 = '4'
    # PESANAN_CHOICES = (
    #     (BARANG1, 'Barang 1'),
    #     (BARANG2, 'Barang 2'),
    #     (BARANG3, 'Barang 3'),
    #     (BARANG4, 'Barang 4')
    # )

    # ALAMAT1 = '1'
    # ALAMAT2 = '2'
    # ALAMAT3 = '3'
    # ALAMAT4 = '4'
    # ALAMAT_CHOICES = (
    #     (ALAMAT1, 'Alamat 1'),
    #     (ALAMAT2, 'Alamat 2'),
    #     (ALAMAT3, 'Alamat 3'),
    #     (ALAMAT4, 'Alamat 4')
    # )

    def __init__(self,no_ktp, *args, **kwargs):
        super(PengirimanForm, self).__init__(*args, **kwargs)
        self.no_ktp = str(no_ktp)
        self.fields['barang_pesanan'] = forms.MultipleChoiceField(label="Barang Pesanan", widget=forms.SelectMultiple(attrs=multiselect_attrs), choices=barang_choices(self.no_ktp))
        self.fields['alamat'] = forms.MultipleChoiceField(label = "Alamat", widget=forms.SelectMultiple(attrs=multiselect_attrs), choices=alamat_choices(self.no_ktp))
    
    tanggal = forms.DateField(label="Tanggal", widget=forms.DateInput(attrs=tanggal_attrs))
    metode = forms.CharField(label="Metode", max_length= 100, widget=forms.TextInput(attrs=metode_attrs))

    