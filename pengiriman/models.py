from django.db import models

# Create your models here.
class Pengiriman(models.Model):
    tanggal=models.DateField()
    metode=models.CharField(max_length=100)

# class BarangPesanan(models.Model):
#     BARANG1 = '1'
#     BARANG2 = '2'
#     BARANG3 = '3'
#     BARANG4 = '4'
#     PESANAN_CHOICES = (
#         (BARANG1, 'Barang 1'),
#         (BARANG2, 'Barang 2'),
#         (BARANG3, 'Barang 3'),
#         (BARANG4, 'Barang 4')
#     )

# class Alamat(models.Model):
#     ALAMAT1 = '1'
#     ALAMAT2 = '2'
#     ALAMAT3 = '3'
#     ALAMAT4 = '4'
#     ALAMAT_CHOICES = (
#         (ALAMAT1, 'Alamat 1'),
#         (ALAMAT2, 'Alamat 2'),
#         (ALAMAT3, 'Alamat 3'),
#         (ALAMAT4, 'Alamat 4')
#     )

# class BarangPengiriman(models.Model):
#     BARANG1 = '1'
#     BARANG2 = '2'
#     BARANG3 = '3'
#     BARANG4 = '4'
#     PENGIRIMAN_CHOICES = (
#         (BARANG1, 'Barang 1'),
#         (BARANG2, 'Barang 2'),
#         (BARANG3, 'Barang 3'),
#         (BARANG4, 'Barang 4')
#     )
