from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'barang'
urlpatterns = [
    path('addbarang', addBarang, name='addBarang'),
    path('updatebarang/<str:id_barang>', updateBarang, name="updateBarang"),
    path('daftarbarang/<str:page>', listBarang, name='listBarang'),
    path('detailbarang/<str:id_barang>', detailBarang, name='detailBarang'),
    path('deletebarang/<str:id_barang>', deleteBarang, name="deletebarang"),
    path('daftarbarang/<str:kategori>/<str:page>', kategoriFiltering, name="filtering"),
    path('daftarbarang/<str:order>/<str:ini>/<str:page>', listOrdered, name="sorting"),

]