from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import BarangForm
from django.urls import reverse
from django.db import connection
from user.models import *
from django.forms import formset_factory
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

def addBarang(request):
    hasil = LevelKeanggotaan.objects.raw('SELECT nama_level FROM LEVEL_KEANGGOTAAN')
    jumlah = len(hasil)
    list_nama_level = [n.nama_level for n in hasil]
    if request.method == 'POST':
        resform = BarangForm(request.POST)
        if resform.is_valid():
            save_barang(resform, list_nama_level)
            return HttpResponseRedirect("/barang/daftarbarang/1")
    forms = BarangForm()
    return render (request, 'barang/tambahbarang.html', {'forms':forms})

def updateBarang(request, id_barang):
    barang = list(Barang.objects.raw('Select * from barang where id_barang = %s', [id_barang]))
    info = list(InfoBarangLevel.objects.raw('select * from info_barang_level where id_barang = %s', [id_barang]))
    barang = {'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item,
        'warna_barang': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi_barang': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'pemilik_penyewa': barang[0].no_ktp_penyewa}
    
    for i in info:
        if i.nama_level == 'BRONZE':
            barang['persen_bronze'] = i.porsi_royalti
            barang['sewa_bronze'] = i.harga_sewa
        if i.nama_level == 'SILVER':
            barang['persen_silver'] = i.porsi_royalti
            barang['sewa_silver'] = i.harga_sewa
        if i.nama_level == 'GOLD':
            barang['persen_gold'] = i.porsi_royalti
            barang['sewa_gold'] = i.harga_sewa
    if request.method == 'POST':
        form = BarangForm(request.POST, initial=barang)
        if form.is_valid() and form.has_changed():
            with connection.cursor() as c:
                id_barang = barang['id_barang']
                if 'id_barang' in form.changed_data:
                    create_barang(form)
                    with connection.cursor() as c:
                        c.execute("DELETE FROM barang WHERE id_barang = %s", [barang])
                else:
                    for updated_column in form.changed_data:
                        if updated_column != 'persen_bronze' or updated_column != 'sewa_bronze' or updated_column != 'persen_silver' or updated_column != 'sewa_silver' or updated_column != 'persen_gold' or updated_column != 'sewa_gold':
                            updated_data = form.cleaned_data.get(updated_column)
                            c.execute('update barang set %s = %s where id_barang = %s' % (updated_column,'%s','%s'), [updated_data, id_barang])
                        else:
                            if updated_column == 'persen_bronze':
                                c.execute("update info_barang_level set porsi_royalti = %s where nama_level = 'bronze' and id_barang = %s", [form.cleaned_data.get('porsi_royalti', form.cleaned_data.get('id_barang'))])
                            if updated_column == 'sewa_bronze':
                                c.execute("update info_barang_level set harga_sewa = %s where nama_level = 'bronze' and id_barang = %s", [form.cleaned_data.get('harga_sewa', form.cleaned_data.get('id_barang'))])
                            if updated_column == 'persen_silver':
                                c.execute("update info_barang_level set porsi_royalti = %s where nama_level = 'silver' and id_barang = %s", [form.cleaned_data.get('porsi_royalti', form.cleaned_data.get('id_barang'))])
                            if updated_column == 'sewa_silver':
                                c.execute("update info_barang_level set harga_sewa = %s where nama_level = 'silver' and id_barang = %s", [form.cleaned_data.get('harga_sewa', form.cleaned_data.get('id_barang'))])
                            if updated_column == 'persen_gold':
                                c.execute("update info_barang_level set porsi_royalti = %s where nama_level = 'gold' and id_barang = %s", [form.cleaned_data.get('porsi_royalti', form.cleaned_data.get('id_barang'))])
                            if updated_column == 'sewa_gold':
                                c.execute("update info_barang_level set harga_sewa = %s where nama_level = 'gold' and id_barang = %s", [form.cleaned_data.get('harga_sewa', form.cleaned_data.get('id_barang'))])
            return HttpResponseRedirect('/barang/daftarbarang/1')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = BarangForm(barang) 
        return render(request, 'barang/updatebarang.html', {'forms': form, 'barang_id': id_barang})

def create_barang(form):
    data_barang = [form.cleaned_data.get('id_barang'), form.cleaned_data.get('nama_item'), form.cleaned_data.get('warna_barang'), 
                            form.cleaned_data.get('url_foto'), form.cleaned_data.get('kondisi_barang'), form.cleaned_data.get('lama_penggunaan'),
                            form.cleaned_data.get('pemilik_penyewa')]
    with connection.cursor() as c:
        c.execute("INSERT INTO BARANG VALUES(%s, %s, %s, %s, %s, %s, %s)", data_barang)
        bronze = [form.cleaned_data.get('id_barang'),"BRONZE",form.cleaned_data.get('sewa_bronze'), form.cleaned_data.get('persen_bronze')]
        silver = [form.cleaned_data.get('id_barang'),"SILVER",form.cleaned_data.get('sewa_silver'), form.cleaned_data.get('persen_silver')]
        gold = [form.cleaned_data.get('id_barang'),"GOLD",form.cleaned_data.get('sewa_gold'), form.cleaned_data.get('persen_gold')]
        c.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s,%s,%s,%s)", bronze)
        c.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s,%s,%s,%s)", silver)
        c.execute("INSERT INTO INFO_BARANG_LEVEL VALUES(%s,%s,%s,%s)", gold)

def listBarang(request, page):
    barang = list(Barang.objects.raw('SELECT * FROM BARANG'))
    data = [{
        'id_barang': i.id_barang,
        'nama_item': i.nama_item,
        'warna': i.warna,
        'kondisi': i.kondisi,
    } for i in barang]
    paginator = Paginator(data, 10)
    try:
        list_barang = paginator.page(page)
    except PageNotAnInteger:
        list_barang = paginator.page(1)
    except EmptyPage:
        list_barang = paginator.page(paginator.num_pages)
    return render(request, 'barang/daftarbarang.html', {'barang': list_barang})

def detailBarang(request, id_barang):
    barang = list(Barang.objects.raw('Select * from barang where id_barang = %s', [id_barang]))
    info = list(InfoBarangLevel.objects.raw('select * from info_barang_level where id_barang = %s', [id_barang]))
    with connection.cursor() as cursor:
        cursor.execute('Select barang_dikirim.review, barang_dikirim.tanggal_review from barang_dikirim where id_barang = %s',[id_barang])
        review_query = cursor.fetchall()
        cursor.execute('Select * from info_barang_level where id_barang = %s',[id_barang])
        info_query = cursor.fetchall()

    barang = {'id_barang': barang[0].id_barang,
        'nama_item': barang[0].nama_item,
        'warna': barang[0].warna,
        'url_foto': barang[0].url_foto,
        'kondisi': barang[0].kondisi,
        'lama_penggunaan': barang[0].lama_penggunaan,
        'no_ktp_penyewa': barang[0].no_ktp_penyewa
	}

    review = [{'review': i[0],
        'tanggal_review': i[1]
    } for i in review_query]


    info = [{'id_barang': i[0],
        'nama_level': i[1],
        'harga_sewa': i[2],
        'porsi_royalti': i[3]
    } for i in info_query]
    response = {'barang': barang, 'review': review, 'info':info}
    return render(request, 'barang/detailbarang.html', response)

def deleteBarang(request, id_barang):
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM BARANG where id_barang = %s', [id_barang]
        )
    return HttpResponseRedirect("/barang/daftarbarang/1")

def save_barang(barang, levels):
    toy = [barang.cleaned_data.get('id_barang'), barang.cleaned_data.get('nama_item'), barang.cleaned_data.get('warna_barang'),
           barang.cleaned_data.get('url_foto'), barang.cleaned_data.get('kondisi_barang'),
           barang.cleaned_data.get('lama_penggunaan'),
           barang.cleaned_data.get('pemilik_penyewa')]
    bronze = [barang.cleaned_data.get('id_barang'), 'BRONZE', barang.cleaned_data.get('sewa_bronze'), barang.cleaned_data.get('persen_bronze')]  
    silver = [barang.cleaned_data.get('id_barang'), 'SILVER', barang.cleaned_data.get('sewa_silver'), barang.cleaned_data.get('persen_silver')]
    gold = [barang.cleaned_data.get('id_barang'), 'GOLD', barang.cleaned_data.get('sewa_gold'), barang.cleaned_data.get('persen_gold')]
    info_datas = [bronze, silver, gold]
    with connection.cursor() as cursor:
        cursor.execute('INSERT INTO BARANG values (%s, %s, %s, %s, %s, %s, %s)', toy)
        for data in info_datas:
            cursor.execute('INSERT INTO INFO_BARANG_LEVEL values (%s, %s, %s, %s)', data)

def kategoriFiltering(request, kategori, page):
    barang = list(Barang.objects.raw('SELECT * FROM BARANG where warna = %s',[kategori]))
    data = [{
        'id_barang': i.id_barang,
        'nama_item': i.nama_item,
        'warna': i.warna,
        'kondisi': i.kondisi,
    } for i in barang]
    paginator = Paginator(data, 10)
    try:
        list_barang = paginator.page(page)
    except PageNotAnInteger:
        list_barang = paginator.page(1)
    except EmptyPage:
        list_barang = paginator.page(paginator.num_pages)
    return render(request, 'barang/daftarbarang.html', {'barang': list_barang})

def listOrdered(request, order, ini, page):
    order_q = order
    ini_q = ini
    query = "SELECT * FROM BARANG order by " + order + " " + ini
    barang = list(Barang.objects.raw(query))
    data = [{
        'id_barang': i.id_barang,
        'nama_item': i.nama_item,
        'warna': i.warna,
        'kondisi': i.kondisi,
    } for i in barang]
    paginator = Paginator(data, 10)
    try:
        list_barang = paginator.page(page)
    except PageNotAnInteger:
        list_barang = paginator.page(1)
    except EmptyPage:
        list_barang = paginator.page(paginator.num_pages)
    return render(request, 'barang/daftarbarang.html', {'barang': list_barang})