from django import forms
from user.models import Anggota, Pengguna, Item
from django.db import connection

ITEM_CHOICES=[
    ('boneka', 'Boneka'),
    ('mahkota', 'Mahkota Princess Merk X'),
    ('masak-masakan', 'Masak-masakan'),
]

def anggota_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT a.no_ktp from anggota a, pengguna p where p.no_ktp = a.no_ktp")
        sini = cursor.fetchall()
        result = [(i[0], i[0]) for i in sini]
        return result
def item_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama from item")
        sini = cursor.fetchall()
        result = [(i[0], i[0]) for i in sini]
        return result
idbarang_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'ID Barang',
}

warnabarang_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Warna',
}

urlfoto_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'URL Foto',
}

kondisibarang_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Kondisi',
}

lamapenggunaan_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Lama Penggunaan',
}

persenbronze_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Persen Royalty',
}

sewabronze_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',

    'placeholder': 'Harga Sewa',
}

persensilver_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Persen Royalty',
}

sewasilver_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Harga Sewa',
}

persengold_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Persen Royalty',
}

sewagold_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder': 'Harga Sewa',
}

namaitem_attrs = {
    'class': 'form-control form-border-pink',
}

pemilikpenyewa_attrs = {
    'class': 'form-control form-border-pink',
}

class BarangForm(forms.Form):
    id_barang = forms.CharField(max_length=50, widget=forms.TextInput(attrs=idbarang_attrs))
    nama_item = forms.CharField(label='Nama item', widget=forms.Select(choices=item_choices(), attrs=namaitem_attrs))
    warna_barang = forms.CharField(max_length=50, widget=forms.TextInput(attrs=warnabarang_attrs))
    url_foto = forms.CharField(max_length=50, widget=forms.TextInput(attrs=urlfoto_attrs))
    kondisi_barang = forms.CharField(max_length=50, widget=forms.TextInput(attrs=kondisibarang_attrs))
    lama_penggunaan = forms.IntegerField(widget=forms.NumberInput(attrs=lamapenggunaan_attrs))
    pemilik_penyewa = forms.CharField(widget=forms.Select(choices=anggota_choices(), attrs=pemilikpenyewa_attrs))
    persen_bronze= forms.IntegerField(widget=forms.NumberInput(attrs=persenbronze_attrs))
    sewa_bronze = forms.IntegerField(widget=forms.NumberInput(attrs=sewabronze_attrs))
    persen_silver = forms.IntegerField(widget=forms.NumberInput(attrs=persensilver_attrs))
    sewa_silver = forms.IntegerField(widget=forms.NumberInput(attrs=sewasilver_attrs))
    persen_gold = forms.IntegerField(widget=forms.NumberInput(attrs=persengold_attrs))
    sewa_gold = forms.IntegerField(widget=forms.NumberInput(attrs=sewagold_attrs))