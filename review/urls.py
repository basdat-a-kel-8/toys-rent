from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'review'
urlpatterns = [
    path('add/', addReview, name='addReview'),
    path('renew', updateReview, name='updateReview'),
    path('lists/<str:page>', listReview, name='listReview'),
    path('delete/<str:id>/<str:id_barang>', deleteReview, name='deleteReview')
]