from django import forms
from .models import Review
from django.core.exceptions import ValidationError
from django.db import connection

multiselect_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
}

review_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Review Barang',
}
def barang_choices():
    with connection.cursor() as c:
        c.execute("select id_barang from barang_pesanan ")
        ids = c.fetchall()
        result = [(i[0], i[0]) for i in ids]
        return result
class ReviewForm(forms.Form):
    # BARANG1 = '1'
    # BARANG2 = '2'
    # BARANG3 = '3'
    # BARANG4 = '4'
    # PENGIRIMAN_CHOICES = (
    #     (BARANG1, 'Barang 1'),
    #     (BARANG2, 'Barang 2'),
    #     (BARANG3, 'Barang 3'),
    #     (BARANG4, 'Barang 4')
    #     )

    barang = forms.MultipleChoiceField(label="Barang Pesanan", widget=forms.SelectMultiple(attrs=multiselect_attrs), choices=barang_choices())
    review = forms.CharField(max_length=1000, label="Review", widget=forms.TextInput(attrs=review_attrs))