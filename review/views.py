from django.shortcuts import render
from .forms import ReviewForm
from user.models import *
from django.db import connection
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect
import calendar
import time
import random, string

# Create your views here.
def addReview(request):
    no_ktp = request.session['no_ktp']
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            time_stamp = time.ctime(calendar.timegm(time.gmtime()))
            create_review(form,no_ktp,time_stamp)
            return HttpResponseRedirect('/review/lists/1')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = ReviewForm()
        return render(request, 'review/tulisReview.html', {'forms':forms})

def updateReview(request):
    return render(request, 'review/updateReview.html')

def listReview(request, page):
    no_ktp=request.session['no_ktp']
    role=request.session['role']
    if role == "admin":
        reviews = list(BarangDikirim.objects.raw('SELECT * FROM BARANG_DIKIRIM'))
        list_reviews = [{
        'id_pengiriman' :i.no_resi,
        'barang' :i.id_barang,
        'review' :i.review,
        'tanggal' :i.tanggal_review,
        } for i in reviews]
    else:
        with connection.cursor() as c:
            c.execute("select b.no_resi, b.id_barang, b.review, b.tanggal_review from barang_dikirim b, pengiriman p, pemesanan m where b.no_resi = p.no_resi and p.id_pemesanan = m.id_pemesanan and no_ktp_pemesan = %s", [no_ktp])
            data = c.fetchall()
            print(data)
            list_reviews=[{
                'id_pengiriman':i[0],
                'barang':i[1],
                'review':i[2],
                'tanggal':i[3],
            }for i in data]
    paginator = Paginator(list_reviews, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    c.close()
    return render(request, 'review/listReview.html',{'reviews':data})

def deleteReview(request,id,id_barang):
    with connection.cursor() as c:
        c.execute("SELECT no_urut FROM BARANG_DIKIRIM WHERE no_resi = %s and id_barang= %s",[id,id_barang])
        no_urut = c.fetchall()
        c.execute("DELETE FROM BARANG_DIKIRIM WHERE no_resi = %s and no_urut =%s",[id,no_urut[0][0]])
        c.close()
        return HttpResponseRedirect("/review/lists/1")

def create_review(form,ktp,time):
    reviews = form.cleaned_data.get('review')
    with connection.cursor() as c:
        # c.execute("INSERT INTO BARANG_DIKIRIM(review) VALUES(%s)", data_item)
        c.execute("SELECT no_resi, bp.id_barang from pengiriman k , pemesanan p , barang_pesanan bp where no_ktp_pemesan=%s and p.id_pemesanan=bp.id_pemesanan and bp.id_pemesanan = k.id_pemesanan", [ktp])
        data=c.fetchall()
        print(data)
        barang = 1
        no_resi = 1
        no_urut = string.digits
        for b in form.cleaned_data.get("barang"):
            print(barang)
            barang = b
        for i in data:
            if i[1] == barang :
                no_resi = i[0]
                c.execute("INSERT INTO BARANG_DIKIRIM VALUES (%s,%s,%s,%s,%s)", [no_resi,barang,time,reviews,no_urut])
            