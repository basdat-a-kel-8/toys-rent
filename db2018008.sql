--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.22
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: db2018008
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO db2018008;

--
-- Name: poin_anggota_changes(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE OR REPLACE FUNCTION public.poin_anggota_changes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
	declare
	level_infos RECORD;
	begin
		for level_infos in
			select nama_level, minimum_poin
			from level_keanggotaan
			order by minimum_poin desc
		loop
			if(level_infos.minimum_poin < new.poin) then
			update anggota
			set level = level_infos.nama_level
			where anggota.no_ktp = new.no_ktp;
			end if;
		end loop;
        return new;
	End;
	$$;


ALTER FUNCTION public.poin_anggota_changes() OWNER TO db2018008;

--
-- Name: poin_level_keanggotaan_changes(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE FUNCTION public.poin_level_keanggotaan_changes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
Begin
Update anggota 
Set level = new.nama_level
Where poin >= new.minimum_poin;
Return new;
End;
$$;


ALTER FUNCTION public.poin_level_keanggotaan_changes() OWNER TO db2018008;

--
-- Name: tambah_harga_sewa(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE FUNCTION public.tambah_harga_sewa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
noKtp VARCHAR(20);
namaLevel VARCHAR(20);
jumlah_harga_sewa INTEGER;
BEGIN
SELECT no_ktp_pemesan INTO noKtp FROM PEMESANAN WHERE PEMESANAN.id_pemesanan = NEW.id_pemesanan;
SELECT level INTO namaLevel FROM ANGGOTA WHERE ANGGOTA.no_ktp = noKtp;
SELECT harga_sewa INTO jumlah_harga_sewa FROM INFO_BARANG_LEVEL WHERE INFO_BARANG_LEVEL.id_barang = NEW.id_barang AND INFO_BARANG_LEVEL.nama_level = namaLevel;
UPDATE PEMESANAN
SET harga_sewa = harga_sewa + jumlah_harga_sewa
WHERE NEW.id_pemesanan = id_pemesanan;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.tambah_harga_sewa() OWNER TO db2018008;

--
-- Name: tambah_ongkos(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE FUNCTION public.tambah_ongkos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
UPDATE
PEMESANAN as P
SET P.ongkos = P.ongkos + NEW.ongkos WHERE NEW.id_pemesanan = P.id_pemesanan;
RETURN NEW;
END;
$$;


ALTER FUNCTION public.tambah_ongkos() OWNER TO db2018008;

--
-- Name: update_kolom_kondisi_barang(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE FUNCTION public.update_kolom_kondisi_barang() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
kondisi_baru text; 
BEGIN 
kondisi_baru := 'sedang disewa anggota'; 
UPDATE BARANG SET kondisi = kondisi_baru
WHERE BARANG.id_barang = NEW.id_barang;
RETURN NEW; 
END; 
$$;


ALTER FUNCTION public.update_kolom_kondisi_barang() OWNER TO db2018008;

--
-- Name: update_poin(); Type: FUNCTION; Schema: public; Owner: db2018008
--

CREATE FUNCTION public.update_poin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
ktp_pemesan VARCHAR(20);
begin
select no_ktp_pemesan into ktp_pemesan
from pemesanan
where new.id_pemesanan = id_pemesanan;
update anggota set poin = poin + (100 * NEW.kuantitas_barang)
where no_ktp = ktp_pemesan;
return new;
end;
$$;


ALTER FUNCTION public.update_poin() OWNER TO db2018008;

--
-- Name: update_poin_penyewa(); Type: FUNCTION; Schema: public; Owner: db2018008
--

<<<<<<< HEAD
CREATE or replace FUNCTION toys_rent.update_poin_penyewa() RETURNS trigger
=======
CREATE or replace FUNCTION public.update_poin_penyewa() RETURNS trigger
>>>>>>> 4a61b722aeb70932c704c513f6ced597fc7d3d57
    LANGUAGE plpgsql
    AS $$
declare
ktp_penyewa VARCHAR(20);
begin
select new.no_ktp_penyewa into ktp_penyewa
from barang
where new.id_barang = id_barang;
update anggota set poin = poin + 100
where no_ktp = ktp_penyewa;
return new;
end;
$$;


ALTER FUNCTION public.update_poin_penyewa() OWNER TO db2018008;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE public.admin OWNER TO db2018008;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE public.level_keanggotaan OWNER TO db2018008;

--
-- Name: pengguna; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE public.pengguna OWNER TO db2018008;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.admin (
    no_ktp character varying(20) NOT NULL
);


ALTER TABLE public.admin OWNER TO db2018008;

--
-- Name: alamat; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.alamat (
    no_ktp_anggota character varying(20) NOT NULL,
    nama character varying(255) NOT NULL,
    jalan character varying(255) NOT NULL,
    nomor integer NOT NULL,
    kota character varying(255) NOT NULL,
    kodepos character varying(10) NOT NULL
);


ALTER TABLE public.alamat OWNER TO db2018008;

--
-- Name: anggota; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.anggota (
    no_ktp character varying(20) NOT NULL,
    poin real NOT NULL,
    level character varying(20)
);


ALTER TABLE public.anggota OWNER TO db2018008;

--
-- Name: barang; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.barang (
    id_barang character varying(10) NOT NULL,
    nama_item character varying(255) NOT NULL,
    warna character varying(50),
    url_foto text,
    kondisi text NOT NULL,
    lama_penggunaan integer,
    no_ktp_penyewa character varying(20) NOT NULL
);


ALTER TABLE public.barang OWNER TO db2018008;

--
-- Name: barang_dikembalikan; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.barang_dikembalikan (
    no_resi character varying(10) NOT NULL,
    id_barang character varying(10),
    no_urut character varying(10) NOT NULL
);


ALTER TABLE public.barang_dikembalikan OWNER TO db2018008;

--
-- Name: barang_dikirim; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.barang_dikirim (
    no_resi character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_review date NOT NULL,
    review text NOT NULL,
    no_urut character varying(10) NOT NULL
);


ALTER TABLE public.barang_dikirim OWNER TO db2018008;

--
-- Name: barang_pesanan; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.barang_pesanan (
    id_pemesanan character varying(10) NOT NULL,
    no_urut character varying(10) NOT NULL,
    id_barang character varying(10),
    tanggal_sewa date NOT NULL,
    lama_sewa integer NOT NULL,
    tanggal_kembali date,
    status character varying(50) NOT NULL
);


ALTER TABLE public.barang_pesanan OWNER TO db2018008;

--
-- Name: chat; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.chat (
    id character varying(15) NOT NULL,
    pesan text NOT NULL,
    date_time timestamp without time zone NOT NULL,
    no_ktp_anggota character varying(20) NOT NULL,
    no_ktp_admin character varying(20) NOT NULL
);


ALTER TABLE public.chat OWNER TO db2018008;

--
-- Name: info_barang_level; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.info_barang_level (
    id_barang character varying(10) NOT NULL,
    nama_level character varying(20) NOT NULL,
    harga_sewa real NOT NULL,
    porsi_royalti real NOT NULL
);


ALTER TABLE public.info_barang_level OWNER TO db2018008;

--
-- Name: item; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.item (
    nama character varying(255) NOT NULL,
    deskripsi text,
    usia_dari integer NOT NULL,
    usia_sampai integer NOT NULL,
    bahan text
);


ALTER TABLE public.item OWNER TO db2018008;

--
-- Name: kategori; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.kategori (
    nama character varying(255) NOT NULL,
    level integer NOT NULL,
    sub_dari character varying(255)
);


ALTER TABLE public.kategori OWNER TO db2018008;

--
-- Name: kategori_item; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.kategori_item (
    nama_item character varying(255) NOT NULL,
    nama_kategori character varying(255) NOT NULL
);


ALTER TABLE public.kategori_item OWNER TO db2018008;

--
-- Name: level_keanggotaan; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.level_keanggotaan (
    nama_level character varying(20) NOT NULL,
    minimum_poin real NOT NULL,
    deskripsi text
);


ALTER TABLE public.level_keanggotaan OWNER TO db2018008;

--
-- Name: pemesanan; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.pemesanan (
    id_pemesanan character varying(10) NOT NULL,
    datetime_pesanan timestamp without time zone NOT NULL,
    kuantitas_barang integer NOT NULL,
    harga_sewa real,
    ongkos real,
    no_ktp_pemesan character varying(20) NOT NULL,
    status character varying(50)
);


ALTER TABLE public.pemesanan OWNER TO db2018008;

--
-- Name: pengembalian; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.pengembalian (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE public.pengembalian OWNER TO db2018008;

--
-- Name: pengguna; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.pengguna (
    no_ktp character varying(20) NOT NULL,
    nama_lengkap character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tanggal_lahir date,
    no_telp character varying(20)
);


ALTER TABLE public.pengguna OWNER TO db2018008;

--
-- Name: pengiriman; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.pengiriman (
    no_resi character varying(10) NOT NULL,
    id_pemesanan character varying(10),
    metode text NOT NULL,
    ongkos real NOT NULL,
    tanggal date NOT NULL,
    no_ktp_anggota character varying(20),
    nama_alamat_anggota character varying(255)
);


ALTER TABLE public.pengiriman OWNER TO db2018008;

--
-- Name: status; Type: TABLE; Schema: public; Owner: db2018008
--

CREATE TABLE public.status (
    nama character varying(50) NOT NULL,
    deskripsi text
);


ALTER TABLE public.status OWNER TO db2018008;

--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.admin (no_ktp) FROM stdin;
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
\.


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.admin (no_ktp) FROM stdin;
1685012399499
1695111896499
1651100890699
1660082579299
1690070372999
1607032122799
1620100974199
1684121769799
1605111218599
1647071347299
\.


--
-- Data for Name: alamat; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.alamat (no_ktp_anggota, nama, jalan, nomor, kota, kodepos) FROM stdin;
1665050425199	Mockingbird	476-6489 A, St.	17	Lamont	O4 1RY
1660031233199	Melvin	Ap #677-8415 Ligula. Rd.	16	Uberlndia	89-265
1652031313299	Michigan	P.O. Box 665, 9514 Sed Rd.	16	Nogales	KV7 1YI
1696030740299	Banding	147-9906 Pede. Road	18	Shahjahanpur	3190
1684012162899	Moulton	P.O. Box 768, 3061 Molestie Rd.	30	Raj Nandgaon	21451
1616092707799	Lawn	8652 Lorem, Av.	22	Auburn	401966
1666111561499	Briar Crest	222-1580 Cum St.	18	Liers	2310 QX
1654091365799	Oak Valley	6349 Magnis Av.	31	Panquehue	978987
1690080707099	Merchant	2559 Aliquet Avenue	19	Nakusp	3101
1647071222999	Cottonwood	969 Purus. Ave	39	Mackay	98192
1682091892499	Charing Cross	Ap #200-5764 Diam Av.	27	Mysore	645718
1655022813899	Marcy	Ap #545-4240 Facilisis, Rd.	44	Rutland	7118
1626111281199	Buell	P.O. Box 842, 5803 Ipsum. Street	45	Arbre	92533
1619100330199	Summit	P.O. Box 317, 9701 Lorem, Street	44	Treppo Carnico	82439
1618081701399	Welch	4594 Gravida Road	28	Merseburg	2719
1653010247599	Waxwing	Ap #269-4844 Faucibus Ave	26	R Verde	1567
1692113095999	Grasskamp	P.O. Box 590, 9209 Dictum St.	35	Versailles	R5H 3J1
1677050256499	Grayhawk	Ap #879-205 Suscipit, Rd.	6	Passau	257567
1675070211499	Tony	Ap #836-4200 Nulla Avenue	30	Budaun	83417
1664050390599	Sherman	456-6828 Diam St.	22	Mddddddling	20233
1638071836799	Hansons	Ap #179-8028 Sit Street	45	Krems an der Donau	98544
1607051844399	Buell	Ap #183-8918 Elementum Ave	38	Lleida	39988
1606091595499	Twin Pines	P.O. Box 879, 7527 Ornare Rd.	3	Detroit	736979
1638082087499	International	7282 Risus. Av.	39	Schiltigheim	IZ2 3SC
1642050910199	Sullivan	840-3270 Metus. Rd.	7	Birmingham	71006
1631022299499	Thackeray	Ap #234-6082 Eu Rd.	29	Rivire-du-Loup	2380
1689092362299	Superior	558-6353 Lobortis, Road	26	Wick	784581
1689021680899	Tennessee	Ap #178-5245 Lorem, Street	27	Senneville	W4 2YB
1698120234499	Beilfuss	P.O. Box 257, 1308 Ut, Avenue	17	Hattersheim am Main	9292
1615011935599	Acker	992-7750 Tincidunt St.	17	Mantova	84105
1646111317399	Arizona	8083 Scelerisque St.	37	Virginal-Samme	39640
1641051558699	American	226-1070 Enim, Street	27	Marbella	8327
1615090330999	South	546-6021 Sed St.	37	Lesve	807357
1661021923199	Hagan	Ap #848-1507 Libero St.	31	Nederokkerzeel	407205
1604080162599	Jackson	455-1090 Nisl Rd.	31	Lac-Serent	21759-162
1670120850299	Bultman	P.O. Box 117, 9004 Orci. Ave	35	Braies/Prags	6223
1609032569999	Marcy	Ap #370-3474 Imperdiet Avenue	8	Baie-Saint-Paul	05-128
1641110636299	Almo	Ap #891-4169 Feugiat. Av.	23	Aiseau	1982
1683031927399	Anderson	527-7659 Nulla St.	39	Shaftesbury	5298
1667041880599	Hanson	P.O. Box 870, 2182 Augue St.	10	Pescantina	8417
1620101983699	Nelson	4415 Nulla Ave	21	Vichy	75283
1639122775999	Caliangt	622 Purus Ave	43	Yumbel	10705
1671121326299	Montana	P.O. Box 777, 1838 Elit, St.	34	Kansas City	51007
1641101587799	Elmside	Ap #810-8011 Proin Road	40	Lens	62933
1601092899599	Carpenter	7073 Turpis Ave	9	Bellevue	61513
1603080783099	Forest Dale	290-7200 Ornare Ave	25	Vito d'Asio	89-865
1678012023599	Butterfield	441 Egestas. Rd.	34	Ludwigsburg	65514
1697062613399	Dryden	P.O. Box 326, 494 Leo, Street	20	Aparecida de Goinia	63646
1647011107599	Walton	5573 Dui, Avenue	30	Rimbey	40408
1694050773099	Cordelia	Ap #355-316 Vitae Road	34	Nieuwmunster	5664
1614081344199	Lighthouse Bay	Ap #811-5676 Nonummy. Road	1	Wilskerke	60067
1674121319999	Sauthoff	814-4997 Nec Road	14	Doues	981416
1621041858199	Pond	5619 Ac Av.	43	Bharatpur	871418
1663102118499	Tennyson	619-2544 Consectetuer, Avenue	24	Hamilton	54284
1614103053699	Eagan	5737 Vitae, Road	33	Wortel	92588
1610020899899	Rusk	7317 In St.	30	Zignago	78226
1611052299599	Lillian	Ap #429-6348 Volutpat Road	25	Salvador	3244
1676040595999	Thompson	987 Nec Rd.	6	King's Lynn	44070-078
1649061934799	Hansons	P.O. Box 770, 7448 Non, St.	9	Lapscheure	484275
1639040163699	Hansons	352-3100 Sagittis Avenue	25	Tuktoyaktuk	7851
1600110143899	Dorton	6810 Curabitur Rd.	43	Waiblingen	7026
1638100504299	Cascade	855-1420 Enim Ave	6	Jodoigne-Souveraine	5112
1692021314399	Springview	P.O. Box 434, 1373 Amet Rd.	2	Vlimmeren	536413
1663080662499	Fairfield	P.O. Box 582, 7555 Faucibus Av.	10	La Estrella	86330
1636060497299	Main	8214 Semper St.	27	Floreffe	6566
1607042618599	Golf View	129-7805 Ultrices. St.	12	Mayerthorpe	273582
1689051327599	Fairfield	P.O. Box 382, 9141 Vulputate, Av.	30	Recco	5533
1686072318499	Meadow Valley	6577 Ipsum. Ave	7	Zaragoza	17780
1658040760699	Namekagon	P.O. Box 242, 6150 Bibendum Avenue	41	Quarona	7716
1670112789499	Kinsman	8556 Varius. Road	3	Longano	5287
1641062533799	Canary	Ap #133-8857 Enim, Road	46	Angol	31721
1628021887599	Briar Crest	796-2314 Gravida Av.	3	Quinte West	5639 ZM
1629041241299	Forest Dale	902-6586 Morbi St.	42	Memphis	23-952
1682082408099	Dixon	2812 Purus St.	30	Pointe-aux-Trembles	28410
1652102354499	Bartillon	Ap #342-3032 Lacus. Rd.	31	Rexton	9181
1671122072799	Brickson Park	1400 Interdum. St.	8	Seraing	942961
1625052383799	Jenna	6214 Pellentesque Rd.	19	Barasat	1015
1614112539899	Muir	Ap #342-3118 Et Av.	11	Ostrowiec wiINTOtokrzyski	8265
1615050466499	Northfield	6522 Nullam Av.	5	Carnoustie	854671
1684091449699	Westridge	Ap #331-3084 Non Road	42	Ribeiro Preto	E0W 4P7
1657052470299	Schurz	P.O. Box 264, 4108 Phasellus Rd.	42	Great Falls	23210
1615030986699	Moland	Ap #428-6057 Eleifend St.	31	Buizingen	8542
1617032381299	3rd	P.O. Box 797, 4471 Cum Rd.	47	Lelystad	444373
1631052448399	John Wall	822-5214 Mattis St.	35	Casanova Elvo	10834
1618011334599	Kedzie	7039 Duis Avenue	19	Warren	9682
1602070815399	Utah	P.O. Box 590, 6441 Hendrerit Avenue	22	Nandyal	61190
1629112962199	Farwell	Ap #556-1558 Non, Rd.	27	Charny	399045
1672080288399	Donald	Ap #735-9530 Vestibulum, St.	50	Waarloos	67218
1660090147199	Merchant	1744 Cras Av.	41	Guelph	57351
1661052642199	Glacier Hill	7072 Turpis Ave	33	Jakarta	16454
\.


--
-- Data for Name: anggota; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.anggota (no_ktp, poin, level) FROM stdin;
1665050425199	5	BRONZE
1660031233199	6	BRONZE
1652031313299	7	BRONZE
1696030740299	8	BRONZE
1684012162899	9	BRONZE
1616092707799	10	BRONZE
1666111561499	11	BRONZE
1654091365799	12	BRONZE
1690080707099	13	BRONZE
1647071222999	14	BRONZE
1682091892499	15	BRONZE
1655022813899	16	BRONZE
1626111281199	17	BRONZE
1619100330199	18	BRONZE
1618081701399	19	BRONZE
1653010247599	20	BRONZE
1692113095999	21	BRONZE
1677050256499	22	BRONZE
1675070211499	23	BRONZE
1664050390599	24	BRONZE
1638071836799	25	BRONZE
1607051844399	26	BRONZE
1606091595499	27	BRONZE
1638082087499	28	BRONZE
1642050910199	29	BRONZE
1631022299499	30	BRONZE
1689092362299	31	BRONZE
1689021680899	32	BRONZE
1698120234499	33	BRONZE
1615011935599	34	BRONZE
1646111317399	350	SILVER
1641051558699	351	SILVER
1615090330999	352	SILVER
1661021923199	353	SILVER
1604080162599	354	SILVER
1670120850299	355	SILVER
1609032569999	356	SILVER
1641110636299	357	SILVER
1683031927399	358	SILVER
1667041880599	359	SILVER
1620101983699	360	SILVER
1639122775999	361	SILVER
1671121326299	362	SILVER
1641101587799	363	SILVER
1601092899599	364	SILVER
1603080783099	365	SILVER
1678012023599	366	SILVER
1697062613399	367	SILVER
1647011107599	368	SILVER
1694050773099	369	SILVER
1614081344199	370	SILVER
1674121319999	371	SILVER
1621041858199	372	SILVER
1663102118499	373	SILVER
1614103053699	374	SILVER
1610020899899	375	SILVER
1611052299599	376	SILVER
1676040595999	377	SILVER
1649061934799	378	SILVER
1639040163699	379	SILVER
1600110143899	900	GOLD
1638100504299	901	GOLD
1692021314399	902	GOLD
1663080662499	903	GOLD
1636060497299	904	GOLD
1607042618599	905	GOLD
1689051327599	906	GOLD
1686072318499	907	GOLD
1658040760699	908	GOLD
1670112789499	909	GOLD
1641062533799	910	GOLD
1628021887599	911	GOLD
1629041241299	912	GOLD
1682082408099	913	GOLD
1652102354499	914	GOLD
1671122072799	915	GOLD
1625052383799	916	GOLD
1614112539899	917	GOLD
1615050466499	918	GOLD
1684091449699	919	GOLD
1657052470299	920	GOLD
1615030986699	921	GOLD
1617032381299	922	GOLD
1631052448399	923	GOLD
1618011334599	924	GOLD
1602070815399	925	GOLD
1629112962199	926	GOLD
1672080288399	927	GOLD
1660090147199	928	GOLD
1661052642199	929	GOLD
\.


--
-- Data for Name: barang; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.barang (id_barang, nama_item, warna, url_foto, kondisi, lama_penggunaan, no_ktp_penyewa) FROM stdin;
1	lorem. Donec elementum, lorem	Purple	http://dummyimage.com/218x141.jpg/cc0000/ffffff	tincidunt tempus risus. Donec egestas.	1	1665050425199
2	sit amet, risus.	Maroon	http://dummyimage.com/237x202.bmp/dddddd/000000	sit amet	2	1660031233199
3	et, commodo at, libero.	Red	http://dummyimage.com/157x184.png/ff4444/ffffff	lorem,	3	1652031313299
4	a sollicitudin orci	Turquoise	http://dummyimage.com/208x245.jpg/dddddd/000000	quam. Curabitur	4	1696030740299
5	sapien imperdiet ornare. In	Blue	http://dummyimage.com/214x192.jpg/cc0000/ffffff	Nullam	5	1684012162899
6	lacus. Cras	Fuscia	http://dummyimage.com/240x117.png/5fa2dd/ffffff	dui. Fusce	6	1616092707799
7	per conubia nostra, per	Red	http://dummyimage.com/190x248.jpg/cc0000/ffffff	magna nec quam.	7	1666111561499
8	facilisis vitae, orci. Phasellus	Blue	http://dummyimage.com/237x241.bmp/ff4444/ffffff	quam. Curabitur	8	1654091365799
9	mollis	Goldenrod	http://dummyimage.com/195x242.jpg/5fa2dd/ffffff	a nunc. In	9	1690080707099
10	molestie tellus. Aenean egestas	Red	http://dummyimage.com/173x133.png/dddddd/000000	ornare	10	1647071222999
11	eu	Indigo	http://dummyimage.com/213x214.bmp/ff4444/ffffff	eu tellus eu	11	1682091892499
12	risus.	Teal	http://dummyimage.com/203x214.png/dddddd/000000	amet	12	1655022813899
13	quis diam luctus lobortis. Class	Turquoise	http://dummyimage.com/176x146.png/dddddd/000000	dapibus gravida. Aliquam tincidunt,	13	1626111281199
14	vel, vulputate eu, odio. Phasellus	Yellow	http://dummyimage.com/155x106.png/cc0000/ffffff	vel, mauris.	14	1619100330199
15	luctus ut, pellentesque	Teal	http://dummyimage.com/155x109.png/5fa2dd/ffffff	pellentesque massa	15	1618081701399
16	at auctor ullamcorper, nisl arcu	Red	http://dummyimage.com/219x149.bmp/cc0000/ffffff	Sed	16	1653010247599
17	metus	Mauv	http://dummyimage.com/174x233.bmp/5fa2dd/ffffff	vehicula et, rutrum eu, ultrices	17	1692113095999
18	mus. Proin	Purple	http://dummyimage.com/239x131.bmp/dddddd/000000	Cum sociis	18	1677050256499
19	dapibus rutrum, justo. Praesent luctus.	Violet	http://dummyimage.com/234x189.png/cc0000/ffffff	vestibulum massa	19	1675070211499
20	enim.	Puce	http://dummyimage.com/105x161.bmp/5fa2dd/ffffff	rutrum magna.	20	1664050390599
21	nunc	Teal	http://dummyimage.com/198x121.bmp/5fa2dd/ffffff	ut,	21	1638071836799
22	tellus	Mauv	http://dummyimage.com/237x131.png/dddddd/000000	lobortis ultrices.	22	1607051844399
23	gravida. Praesent eu	Puce	http://dummyimage.com/246x112.png/cc0000/ffffff	eu	23	1606091595499
24	neque tellus,	Indigo	http://dummyimage.com/228x184.bmp/dddddd/000000	hendrerit. Donec porttitor	24	1638082087499
25	vulputate	Maroon	http://dummyimage.com/190x238.png/5fa2dd/ffffff	euismod in,	25	1642050910199
26	lorem. Donec elementum, lorem	Teal	http://dummyimage.com/159x212.jpg/dddddd/000000	penatibus	26	1631022299499
27	sit amet, risus.	Red	http://dummyimage.com/213x179.bmp/5fa2dd/ffffff	cursus a,	27	1689092362299
28	et, commodo at, libero.	Pink	http://dummyimage.com/148x138.jpg/dddddd/000000	ac mattis ornare, lectus ante	28	1689021680899
29	a sollicitudin orci	Crimson	http://dummyimage.com/209x244.jpg/dddddd/000000	Aenean	29	1698120234499
30	sapien imperdiet ornare. In	Pink	http://dummyimage.com/187x123.jpg/cc0000/ffffff	congue a,	30	1615011935599
31	lacus. Cras	Indigo	http://dummyimage.com/249x146.bmp/dddddd/000000	a,	31	1646111317399
32	per conubia nostra, per	Khaki	http://dummyimage.com/186x213.png/ff4444/ffffff	luctus vulputate, nisi sem	32	1641051558699
33	facilisis vitae, orci. Phasellus	Orange	http://dummyimage.com/247x244.bmp/5fa2dd/ffffff	magnis dis parturient montes,	33	1615090330999
34	mollis	Fuscia	http://dummyimage.com/211x126.jpg/ff4444/ffffff	rutrum urna,	34	1661021923199
35	molestie tellus. Aenean egestas	Mauv	http://dummyimage.com/112x136.png/cc0000/ffffff	massa. Integer vitae	35	1604080162599
36	eu	Indigo	http://dummyimage.com/245x117.jpg/ff4444/ffffff	aliquam	36	1670120850299
37	risus.	Red	http://dummyimage.com/175x176.jpg/5fa2dd/ffffff	commodo hendrerit.	37	1609032569999
38	quis diam luctus lobortis. Class	Crimson	http://dummyimage.com/250x214.jpg/ff4444/ffffff	dignissim. Maecenas	38	1641110636299
39	vel, vulputate eu, odio. Phasellus	Violet	http://dummyimage.com/220x227.png/5fa2dd/ffffff	eros	39	1683031927399
40	luctus ut, pellentesque	Violet	http://dummyimage.com/204x195.jpg/5fa2dd/ffffff	montes,	40	1667041880599
41	at auctor ullamcorper, nisl arcu	Green	http://dummyimage.com/153x189.bmp/dddddd/000000	molestie tellus. Aenean egestas hendrerit	41	1620101983699
42	metus	Puce	http://dummyimage.com/124x226.bmp/ff4444/ffffff	Mauris molestie	42	1639122775999
43	mus. Proin	Mauv	http://dummyimage.com/137x152.png/dddddd/000000	Donec elementum,	43	1671121326299
44	dapibus rutrum, justo. Praesent luctus.	Maroon	http://dummyimage.com/236x203.jpg/5fa2dd/ffffff	egestas	44	1641101587799
45	enim.	Indigo	http://dummyimage.com/120x170.jpg/5fa2dd/ffffff	nisi sem	45	1601092899599
46	nunc	Orange	http://dummyimage.com/240x212.bmp/cc0000/ffffff	amet metus.	46	1603080783099
47	tellus	Yellow	http://dummyimage.com/203x141.bmp/ff4444/ffffff	in,	47	1678012023599
48	gravida. Praesent eu	Red	http://dummyimage.com/139x162.bmp/dddddd/000000	fames ac	48	1697062613399
49	neque tellus,	Puce	http://dummyimage.com/152x171.png/dddddd/000000	et nunc. Quisque ornare	49	1647011107599
50	vulputate	Khaki	http://dummyimage.com/181x178.jpg/ff4444/ffffff	vitae, aliquet nec, imperdiet nec,	50	1694050773099
51	lorem. Donec elementum, lorem	Green	http://dummyimage.com/119x207.png/dddddd/000000	cursus. Nunc	51	1614081344199
52	sit amet, risus.	Indigo	http://dummyimage.com/203x223.jpg/dddddd/000000	adipiscing	52	1674121319999
53	et, commodo at, libero.	Green	http://dummyimage.com/129x142.jpg/5fa2dd/ffffff	Nulla dignissim. Maecenas ornare	53	1621041858199
54	a sollicitudin orci	Yellow	http://dummyimage.com/208x234.png/5fa2dd/ffffff	molestie	54	1663102118499
55	sapien imperdiet ornare. In	Khaki	http://dummyimage.com/114x173.png/ff4444/ffffff	molestie	55	1614103053699
56	lacus. Cras	Puce	http://dummyimage.com/151x101.jpg/ff4444/ffffff	odio, auctor vitae,	56	1610020899899
57	per conubia nostra, per	Aquamarine	http://dummyimage.com/214x102.bmp/5fa2dd/ffffff	diam.	57	1611052299599
58	facilisis vitae, orci. Phasellus	Blue	http://dummyimage.com/151x163.png/dddddd/000000	purus ac tellus. Suspendisse	58	1676040595999
59	mollis	Purple	http://dummyimage.com/134x117.bmp/cc0000/ffffff	magna. Ut	59	1649061934799
60	molestie tellus. Aenean egestas	Violet	http://dummyimage.com/234x148.bmp/5fa2dd/ffffff	nulla magna,	60	1639040163699
61	eu	Puce	http://dummyimage.com/131x241.png/ff4444/ffffff	neque et	61	1600110143899
62	risus.	Teal	http://dummyimage.com/214x120.png/cc0000/ffffff	neque non quam. Pellentesque habitant	62	1638100504299
63	quis diam luctus lobortis. Class	Indigo	http://dummyimage.com/109x205.jpg/dddddd/000000	nibh sit amet orci. Ut	63	1692021314399
64	vel, vulputate eu, odio. Phasellus	Yellow	http://dummyimage.com/232x198.png/cc0000/ffffff	dictum cursus. Nunc mauris	64	1663080662499
65	luctus ut, pellentesque	Yellow	http://dummyimage.com/238x226.bmp/cc0000/ffffff	elementum	65	1636060497299
66	at auctor ullamcorper, nisl arcu	Goldenrod	http://dummyimage.com/214x186.jpg/dddddd/000000	tristique aliquet. Phasellus fermentum	66	1607042618599
67	metus	Puce	http://dummyimage.com/236x222.png/ff4444/ffffff	ante ipsum primis	67	1689051327599
68	mus. Proin	Orange	http://dummyimage.com/122x144.bmp/ff4444/ffffff	Cras dictum ultricies	68	1686072318499
69	dapibus rutrum, justo. Praesent luctus.	Goldenrod	http://dummyimage.com/131x235.jpg/ff4444/ffffff	sed orci lobortis augue scelerisque	69	1658040760699
70	enim.	Yellow	http://dummyimage.com/124x232.jpg/ff4444/ffffff	risus	70	1670112789499
71	nunc	Mauv	http://dummyimage.com/185x115.bmp/dddddd/000000	molestie dapibus	71	1641062533799
72	tellus	Teal	http://dummyimage.com/200x195.bmp/cc0000/ffffff	enim mi tempor lorem,	72	1628021887599
73	gravida. Praesent eu	Blue	http://dummyimage.com/160x230.jpg/ff4444/ffffff	sit amet metus. Aliquam erat	73	1629041241299
74	neque tellus,	Pink	http://dummyimage.com/218x157.bmp/cc0000/ffffff	ornare.	74	1682082408099
75	vulputate	Goldenrod	http://dummyimage.com/118x199.jpg/5fa2dd/ffffff	mattis. Cras eget nisi dictum	75	1652102354499
76	lorem. Donec elementum, lorem	Puce	http://dummyimage.com/186x188.jpg/5fa2dd/ffffff	arcu. Vestibulum ut	76	1671122072799
77	sit amet, risus.	Orange	http://dummyimage.com/143x192.bmp/dddddd/000000	egestas a, dui.	77	1625052383799
78	et, commodo at, libero.	Aquamarine	http://dummyimage.com/119x188.jpg/dddddd/000000	vestibulum, neque sed dictum	78	1614112539899
79	a sollicitudin orci	Mauv	http://dummyimage.com/187x192.png/dddddd/000000	lectus, a	79	1615050466499
80	sapien imperdiet ornare. In	Crimson	http://dummyimage.com/141x141.bmp/cc0000/ffffff	lectus	80	1684091449699
81	lacus. Cras	Crimson	http://dummyimage.com/142x166.png/cc0000/ffffff	a sollicitudin orci sem eget	81	1657052470299
82	per conubia nostra, per	Yellow	http://dummyimage.com/102x208.png/dddddd/000000	eu erat	82	1615030986699
83	facilisis vitae, orci. Phasellus	Aquamarine	http://dummyimage.com/166x204.png/dddddd/000000	adipiscing. Mauris molestie	83	1617032381299
84	mollis	Green	http://dummyimage.com/128x107.jpg/5fa2dd/ffffff	euismod	84	1631052448399
85	molestie tellus. Aenean egestas	Mauv	http://dummyimage.com/229x230.jpg/5fa2dd/ffffff	non, egestas a, dui.	85	1618011334599
86	eu	Red	http://dummyimage.com/211x203.bmp/5fa2dd/ffffff	consectetuer rhoncus. Nullam velit dui,	86	1602070815399
87	risus.	Blue	http://dummyimage.com/236x123.bmp/5fa2dd/ffffff	luctus	87	1629112962199
88	quis diam luctus lobortis. Class	Red	http://dummyimage.com/198x250.jpg/dddddd/000000	ullamcorper magna. Sed	88	1672080288399
89	vel, vulputate eu, odio. Phasellus	Goldenrod	http://dummyimage.com/175x244.jpg/ff4444/ffffff	dui, nec tempus mauris erat	89	1660090147199
90	luctus ut, pellentesque	Teal	http://dummyimage.com/238x108.bmp/cc0000/ffffff	montes, nascetur ridiculus mus.	90	1661052642199
91	at auctor ullamcorper, nisl arcu	Goldenrod	http://dummyimage.com/183x210.bmp/dddddd/000000	Donec consectetuer mauris id sapien.	91	1657052470299
92	metus	Pink	http://dummyimage.com/144x157.png/cc0000/ffffff	sed, hendrerit a,	92	1615030986699
93	mus. Proin	Aquamarine	http://dummyimage.com/191x131.jpg/5fa2dd/ffffff	a felis ullamcorper viverra. Maecenas	93	1617032381299
94	dapibus rutrum, justo. Praesent luctus.	Blue	http://dummyimage.com/178x199.jpg/5fa2dd/ffffff	Quisque	94	1631052448399
95	enim.	Turquoise	http://dummyimage.com/117x237.bmp/5fa2dd/ffffff	odio. Aliquam vulputate	95	1618011334599
96	nunc	Blue	http://dummyimage.com/215x222.bmp/cc0000/ffffff	velit eget laoreet posuere,	96	1602070815399
97	tellus	Blue	http://dummyimage.com/186x182.bmp/cc0000/ffffff	ac urna. Ut tincidunt vehicula	97	1629112962199
98	gravida. Praesent eu	Khaki	http://dummyimage.com/178x165.bmp/5fa2dd/ffffff	molestie. Sed id	98	1672080288399
99	neque tellus,	Violet	http://dummyimage.com/220x158.bmp/dddddd/000000	commodo ipsum.	99	1660090147199
100	vulputate	Pink	http://dummyimage.com/135x150.jpg/cc0000/ffffff	sedang disewa anggota	100	1661052642199
\.


--
-- Data for Name: barang_dikembalikan; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.barang_dikembalikan (no_resi, id_barang, no_urut) FROM stdin;
VPN8883379	1	1
LPU6571362	2	2
JCL2238630	3	3
QOJ7774351	4	4
MPD1748146	5	5
JWP5352231	6	6
SXN8470256	7	7
MHZ2398148	8	8
HKP2095058	9	9
NKH8313913	10	10
XOZ6930390	11	11
UMB3513497	12	12
RUK2602423	13	13
QYM7492758	14	14
EZG0726229	15	15
SYJ4042072	16	16
QAB2182681	17	17
EHJ6424027	18	18
VGQ1606101	19	19
FAY9240972	20	20
FAY9240973	21	21
FAY9240974	22	22
FAY9240975	23	23
FAY9240976	24	24
FAY9240977	25	25
FAY9240978	26	26
FAY9240979	27	27
FAY9240980	28	28
FAY9240981	29	29
FAY9240982	30	30
FAY9240983	31	31
FAY9240984	32	32
FAY9240985	33	33
FAY9240986	34	34
FAY9240987	35	35
FAY9240988	36	36
FAY9240989	37	37
FAY9240990	38	38
FAY9240991	39	39
FAY9240992	40	40
\.


--
-- Data for Name: barang_dikirim; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.barang_dikirim (no_resi, id_barang, tanggal_review, review, no_urut) FROM stdin;
VPN8883379	1	2019-09-04	tincidunt tempus risus. Donec egestas.	1
LPU6571362	2	2019-08-01	sit amet	2
JCL2238630	3	2019-09-25	lorem,	3
QOJ7774351	4	2019-10-27	quam. Curabitur	4
MPD1748146	5	2019-10-10	Nullam	5
JWP5352231	6	2019-09-26	dui. Fusce	6
SXN8470256	7	2019-10-10	magna nec quam.	7
MHZ2398148	8	2019-08-20	quam. Curabitur	8
HKP2095058	9	2019-09-16	a nunc. In	9
NKH8313913	10	2019-08-13	ornare	10
XOZ6930390	11	2019-08-07	eu tellus eu	11
UMB3513497	12	2019-10-28	amet	12
RUK2602423	13	2019-10-16	dapibus gravida. Aliquam tincidunt,	13
QYM7492758	14	2019-08-03	vel, mauris.	14
EZG0726229	15	2019-10-09	pellentesque massa	15
SYJ4042072	16	2019-08-15	Sed	16
QAB2182681	17	2019-08-30	vehicula et, rutrum eu, ultrices	17
EHJ6424027	18	2019-09-01	Cum sociis	18
VGQ1606101	19	2019-09-02	vestibulum massa	19
FAY9240972	20	2019-08-19	rutrum magna.	20
FAY9240973	21	2019-09-08	ut,	21
FAY9240974	22	2019-08-11	lobortis ultrices.	22
FAY9240975	23	2019-08-13	eu	23
FAY9240976	24	2019-09-11	hendrerit. Donec porttitor	24
FAY9240977	25	2019-08-26	euismod in,	25
FAY9240978	26	2019-08-23	penatibus	26
FAY9240979	27	2019-08-09	cursus a,	27
FAY9240980	28	2019-09-28	ac mattis ornare, lectus ante	28
FAY9240981	29	2019-09-20	Aenean	29
FAY9240982	30	2019-08-19	congue a,	30
FAY9240983	31	2019-10-11	a,	31
FAY9240984	32	2019-09-05	luctus vulputate, nisi sem	32
FAY9240985	33	2019-10-07	magnis dis parturient montes,	33
FAY9240986	34	2019-10-20	rutrum urna,	34
FAY9240987	35	2019-09-20	massa. Integer vitae	35
FAY9240988	36	2019-10-16	aliquam	36
FAY9240989	37	2019-09-28	commodo hendrerit.	37
FAY9240990	38	2019-08-04	dignissim. Maecenas	38
FAY9240991	39	2019-08-09	eros	39
FAY9240992	40	2019-08-03	montes,	40
\.


--
-- Data for Name: barang_pesanan; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.barang_pesanan (id_pemesanan, no_urut, id_barang, tanggal_sewa, lama_sewa, tanggal_kembali, status) FROM stdin;
4358786618	1	1	2019-03-04	1	2019-03-05	SEDANG DIKONFIRMASI
3914039361	2	2	2019-04-09	2	2019-04-11	SEDANG DIKONFIRMASI
4439622607	3	3	2019-02-16	3	2019-02-19	SEDANG DIKONFIRMASI
6287741309	4	4	2019-03-05	4	2019-03-09	SEDANG DIKONFIRMASI
9150399314	5	5	2019-04-19	5	2019-04-24	SEDANG DIKONFIRMASI
1722807687	6	6	2019-02-13	6	2019-02-19	SEDANG DIKONFIRMASI
3886354008	7	7	2019-04-12	7	2019-04-19	SEDANG DIKONFIRMASI
4893997866	8	8	2019-04-17	1	2019-04-18	SEDANG DIKONFIRMASI
2716502501	9	9	2019-04-08	2	2019-04-10	SEDANG DIKONFIRMASI
2956957813	10	10	2019-04-15	3	2019-04-18	SEDANG DIKONFIRMASI
9034274004	11	11	2019-04-20	4	2019-04-24	SEDANG DIKONFIRMASI
3988911615	12	12	2019-02-19	5	2019-02-24	SEDANG DIKONFIRMASI
5363235951	13	13	2019-04-24	6	2019-04-30	SEDANG DIKONFIRMASI
5149585726	14	14	2019-02-28	7	2019-03-07	SEDANG DIKONFIRMASI
2713578175	15	15	2019-04-05	1	2019-04-06	SEDANG DIKONFIRMASI
6782050293	16	16	2019-04-26	2	2019-04-28	SEDANG DIKONFIRMASI
9347267287	17	17	2019-03-12	3	2019-03-15	SEDANG DIKONFIRMASI
2023033586	18	18	2019-02-11	4	2019-02-15	SEDANG DIKONFIRMASI
2881534376	19	19	2019-02-24	5	2019-03-01	SEDANG DIKONFIRMASI
3837372537	20	20	2019-03-04	6	2019-03-10	SEDANG DIKONFIRMASI
9006328618	21	21	2019-03-13	7	2019-03-20	SEDANG DIKONFIRMASI
988293536	22	22	2019-04-12	1	2019-04-13	SEDANG DIKONFIRMASI
378785931	23	23	2019-02-11	2	2019-02-13	SEDANG DIKONFIRMASI
6717097140	24	24	2019-03-21	3	2019-03-24	SEDANG DIKONFIRMASI
523614551	25	25	2019-04-23	4	2019-04-27	SEDANG DIKONFIRMASI
3396141148	26	26	2019-04-17	5	2019-04-22	SEDANG DIKONFIRMASI
1249492157	27	27	2019-03-27	6	2019-04-02	SEDANG DIKONFIRMASI
2519904208	28	28	2019-03-08	7	2019-03-15	SEDANG DIKONFIRMASI
8730457513	29	29	2019-02-03	1	2019-02-04	SEDANG DIKONFIRMASI
9222573471	30	30	2019-03-24	2	2019-03-26	SEDANG DIKONFIRMASI
7076376695	31	31	2019-03-29	3	2019-04-01	SEDANG DIKONFIRMASI
236916904	32	32	2019-04-27	4	2019-05-01	SEDANG DIKONFIRMASI
332962296	33	33	2019-04-06	5	2019-04-11	SEDANG DIKONFIRMASI
4099390772	34	34	2019-02-28	6	2019-03-06	SEDANG DIKONFIRMASI
6869799660	35	35	2019-03-16	7	2019-03-23	SEDANG DIKONFIRMASI
5195025134	36	36	2019-02-07	1	2019-02-08	SEDANG DIKONFIRMASI
6379737650	37	37	2019-04-06	2	2019-04-08	SEDANG DIKONFIRMASI
9873927840	38	38	2019-04-10	3	2019-04-13	SEDANG DIKONFIRMASI
1536799653	39	39	2019-04-14	4	2019-04-18	SEDANG DIKONFIRMASI
6602767755	40	40	2019-04-22	5	2019-04-27	SEDANG DIKONFIRMASI
9525456463	41	41	2019-02-08	6	2019-02-14	SEDANG DIKONFIRMASI
3569575977	42	42	2019-02-08	7	2019-02-15	SEDANG DIKONFIRMASI
6264163384	43	43	2019-02-08	1	2019-02-09	SEDANG DIKONFIRMASI
9076717699	44	44	2019-04-03	2	2019-04-05	SEDANG DIKONFIRMASI
3562680244	45	45	2019-04-13	3	2019-04-16	SEDANG DIKONFIRMASI
5678778056	46	46	2019-02-20	4	2019-02-24	SEDANG DIKONFIRMASI
652997198	47	47	2019-02-19	5	2019-02-24	SEDANG DIKONFIRMASI
4779757363	48	48	2019-02-22	6	2019-02-28	SEDANG DIKONFIRMASI
7282724776	49	49	2019-04-08	7	2019-04-15	SEDANG DIKONFIRMASI
1009692399	50	50	2019-04-13	1	2019-04-14	SEDANG DIKONFIRMASI
4358786618	51	51	2019-03-13	2	2019-03-15	SEDANG DIKONFIRMASI
3914039361	52	52	2019-04-20	3	2019-04-23	SEDANG DIKONFIRMASI
4439622607	53	53	2019-03-10	4	2019-03-14	SEDANG DIKONFIRMASI
6287741309	54	54	2019-04-04	5	2019-04-09	SEDANG DIKONFIRMASI
9150399314	55	55	2019-02-04	6	2019-02-10	SEDANG DIKONFIRMASI
1722807687	56	56	2019-03-17	7	2019-03-24	SEDANG DIKONFIRMASI
3886354008	57	57	2019-02-28	1	2019-03-01	SEDANG DIKONFIRMASI
4893997866	58	58	2019-04-16	2	2019-04-18	SEDANG DIKONFIRMASI
2716502501	59	59	2019-02-12	3	2019-02-15	SEDANG DIKONFIRMASI
2956957813	60	60	2019-02-20	4	2019-02-24	SEDANG DIKONFIRMASI
9034274004	61	61	2019-04-18	5	2019-04-23	SEDANG DIKONFIRMASI
3988911615	62	62	2019-04-29	6	2019-05-05	SEDANG DIKONFIRMASI
5363235951	63	63	2019-03-10	7	2019-03-17	SEDANG DIKONFIRMASI
5149585726	64	64	2019-04-06	1	2019-04-07	SEDANG DIKONFIRMASI
2713578175	65	65	2019-03-04	2	2019-03-06	SEDANG DIKONFIRMASI
6782050293	66	66	2019-04-09	3	2019-04-12	SEDANG DIKONFIRMASI
9347267287	67	67	2019-02-10	4	2019-02-14	SEDANG DIKONFIRMASI
2023033586	68	68	2019-03-26	5	2019-03-31	SEDANG DIKONFIRMASI
2881534376	69	69	2019-04-13	6	2019-04-19	SEDANG DIKONFIRMASI
3837372537	70	70	2019-04-22	7	2019-04-29	SEDANG DIKONFIRMASI
9006328618	71	71	2019-03-10	1	2019-03-11	SEDANG DIKONFIRMASI
988293536	72	72	2019-04-02	2	2019-04-04	SEDANG DIKONFIRMASI
378785931	73	73	2019-04-04	3	2019-04-07	SEDANG DIKONFIRMASI
6717097140	74	74	2019-04-15	4	2019-04-19	SEDANG DIKONFIRMASI
523614551	75	75	2019-02-19	5	2019-02-24	SEDANG DIKONFIRMASI
3396141148	76	76	2019-02-12	6	2019-02-18	SEDANG DIKONFIRMASI
1249492157	77	77	2019-02-18	7	2019-02-25	SEDANG DIKONFIRMASI
2519904208	78	78	2019-02-25	1	2019-02-26	SEDANG DIKONFIRMASI
8730457513	79	79	2019-04-27	2	2019-04-29	SEDANG DIKONFIRMASI
9222573471	80	80	2019-03-03	3	2019-03-06	SEDANG DIKONFIRMASI
7076376695	81	81	2019-03-22	4	2019-03-26	SEDANG DIKONFIRMASI
236916904	82	82	2019-04-29	5	2019-05-04	SEDANG DIKONFIRMASI
332962296	83	83	2019-04-06	6	2019-04-12	SEDANG DIKONFIRMASI
4099390772	84	84	2019-03-17	7	2019-03-24	SEDANG DIKONFIRMASI
6869799660	85	85	2019-03-12	1	2019-03-13	SEDANG DIKONFIRMASI
5195025134	86	86	2019-03-24	2	2019-03-26	SEDANG DIKONFIRMASI
6379737650	87	87	2019-03-08	3	2019-03-11	SEDANG DIKONFIRMASI
9873927840	88	88	2019-02-26	4	2019-03-02	SEDANG DIKONFIRMASI
1536799653	89	89	2019-04-26	5	2019-05-01	SEDANG DIKONFIRMASI
6602767755	90	90	2019-02-01	6	2019-02-07	SEDANG DIKONFIRMASI
9525456463	91	91	2019-03-10	7	2019-03-17	SEDANG DIKONFIRMASI
3569575977	92	92	2019-03-05	1	2019-03-06	SEDANG DIKONFIRMASI
6264163384	93	93	2019-03-24	2	2019-03-26	SEDANG DIKONFIRMASI
9076717699	94	94	2019-03-21	3	2019-03-24	SEDANG DIKONFIRMASI
3562680244	95	95	2019-03-23	4	2019-03-27	SEDANG DIKONFIRMASI
5678778056	96	96	2019-04-29	5	2019-05-04	SEDANG DIKONFIRMASI
652997198	97	97	2019-04-16	6	2019-04-22	SEDANG DIKONFIRMASI
4779757363	98	98	2019-02-14	7	2019-02-21	SEDANG DIKONFIRMASI
7282724776	99	99	2019-02-27	1	2019-02-28	SEDANG DIKONFIRMASI
1009692399	100	100	2019-04-08	2	2019-04-10	SEDANG DIKONFIRMASI
1009692399	101	100	2019-05-19	3	2019-05-20	SEDANG DIKONFIRMASI
\.


--
-- Data for Name: chat; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.chat (id, pesan, date_time, no_ktp_anggota, no_ktp_admin) FROM stdin;
1	tincidunt tempus risus. Donec egestas.	2018-12-22 00:00:00	1665050425199	1685012399499
2	sit amet	2019-05-23 00:00:00	1660031233199	1695111896499
3	lorem,	2018-09-26 00:00:00	1652031313299	1651100890699
4	quam. Curabitur	2019-08-14 00:00:00	1696030740299	1660082579299
5	Nullam	2018-12-26 00:00:00	1684012162899	1690070372999
6	dui. Fusce	2019-04-04 00:00:00	1616092707799	1607032122799
7	magna nec quam.	2019-01-26 00:00:00	1666111561499	1620100974199
8	quam. Curabitur	2019-06-03 00:00:00	1654091365799	1684121769799
9	a nunc. In	2018-08-15 00:00:00	1690080707099	1605111218599
10	ornare	2019-09-18 00:00:00	1647071222999	1647071347299
11	eu tellus eu	2019-02-27 00:00:00	1682091892499	1685012399499
12	amet	2019-02-18 00:00:00	1655022813899	1695111896499
13	dapibus gravida. Aliquam tincidunt,	2019-11-24 00:00:00	1626111281199	1651100890699
14	vel, mauris.	2019-05-15 00:00:00	1619100330199	1660082579299
15	pellentesque massa	2019-10-12 00:00:00	1618081701399	1690070372999
16	Sed	2018-08-14 00:00:00	1653010247599	1607032122799
17	vehicula et, rutrum eu, ultrices	2019-06-29 00:00:00	1692113095999	1620100974199
18	Cum sociis	2018-05-30 00:00:00	1677050256499	1684121769799
19	vestibulum massa	2019-01-10 00:00:00	1675070211499	1605111218599
20	rutrum magna.	2018-06-15 00:00:00	1664050390599	1647071347299
21	ut,	2020-02-20 00:00:00	1638071836799	1685012399499
22	lobortis ultrices.	2018-11-27 00:00:00	1607051844399	1695111896499
23	eu	2019-01-27 00:00:00	1606091595499	1651100890699
24	hendrerit. Donec porttitor	2018-12-06 00:00:00	1638082087499	1660082579299
25	euismod in,	2019-12-19 00:00:00	1642050910199	1690070372999
26	penatibus	2019-09-30 00:00:00	1631022299499	1607032122799
27	cursus a,	2018-12-26 00:00:00	1689092362299	1620100974199
28	ac mattis ornare, lectus ante	2019-11-13 00:00:00	1689021680899	1684121769799
29	Aenean	2019-05-20 00:00:00	1698120234499	1605111218599
30	congue a,	2019-05-30 00:00:00	1615011935599	1647071347299
31	a,	2018-06-08 00:00:00	1646111317399	1685012399499
32	luctus vulputate, nisi sem	2019-03-09 00:00:00	1641051558699	1695111896499
33	magnis dis parturient montes,	2020-02-09 00:00:00	1615090330999	1651100890699
34	rutrum urna,	2018-07-15 00:00:00	1661021923199	1660082579299
35	massa. Integer vitae	2018-12-12 00:00:00	1604080162599	1690070372999
36	aliquam	2020-03-28 00:00:00	1670120850299	1607032122799
37	commodo hendrerit.	2018-05-16 00:00:00	1609032569999	1620100974199
38	dignissim. Maecenas	2019-06-04 00:00:00	1641110636299	1684121769799
39	eros	2018-06-22 00:00:00	1683031927399	1605111218599
40	montes,	2019-04-17 00:00:00	1667041880599	1647071347299
41	molestie tellus. Aenean egestas hendrerit	2019-05-15 00:00:00	1620101983699	1685012399499
42	Mauris molestie	2019-03-04 00:00:00	1639122775999	1695111896499
43	Donec elementum,	2018-08-22 00:00:00	1671121326299	1651100890699
44	egestas	2019-11-22 00:00:00	1641101587799	1660082579299
45	nisi sem	2018-12-09 00:00:00	1601092899599	1690070372999
46	amet metus.	2018-08-12 00:00:00	1603080783099	1607032122799
47	in,	2019-08-19 00:00:00	1678012023599	1620100974199
48	fames ac	2019-09-05 00:00:00	1697062613399	1684121769799
49	et nunc. Quisque ornare	2019-07-16 00:00:00	1647011107599	1605111218599
50	dictum cursus. Nunc mauris elit, dictum eu,	2019-05-13 00:00:00	1694050773099	1647071347299
\.


--
-- Data for Name: info_barang_level; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.info_barang_level (id_barang, nama_level, harga_sewa, porsi_royalti) FROM stdin;
1	GOLD	78268	77768
2	SILVER	98833	98333
3	BRONZE	11690	11190
4	GOLD	18503	18003
5	SILVER	78029	77529
6	BRONZE	14800	14300
7	GOLD	74540	74040
8	SILVER	71149	70649
9	BRONZE	30637	30137
10	GOLD	85523	85023
11	SILVER	83849	83349
12	BRONZE	2584	2084
13	GOLD	58463	57963
14	SILVER	59957	59457
15	BRONZE	32789	32289
16	GOLD	93326	92826
17	SILVER	55789	55289
18	BRONZE	52522	52022
19	GOLD	36784	36284
20	SILVER	27624	27124
21	BRONZE	19550	19050
22	GOLD	13576	13076
23	SILVER	76266	75766
24	BRONZE	17069	16569
25	GOLD	37737	37237
26	SILVER	88701	88201
27	BRONZE	89373	88873
28	GOLD	11980	11480
29	SILVER	34597	34097
30	BRONZE	29264	28764
31	GOLD	54863	54363
32	SILVER	93263	92763
33	BRONZE	20358	19858
34	GOLD	56916	56416
35	SILVER	32395	31895
36	BRONZE	58556	58056
37	GOLD	57131	56631
38	SILVER	29000	28500
39	BRONZE	19876	19376
40	GOLD	59056	58556
41	SILVER	97484	96984
42	BRONZE	69047	68547
43	GOLD	64514	64014
44	SILVER	53949	53449
45	BRONZE	80182	79682
46	GOLD	74736	74236
47	SILVER	67794	67294
48	BRONZE	27414	26914
49	GOLD	7588	7088
50	SILVER	8170	7670
51	BRONZE	52529	52029
52	GOLD	26161	25661
53	SILVER	11286	10786
54	BRONZE	2213	1713
55	GOLD	74784	74284
56	SILVER	83980	83480
57	BRONZE	71689	71189
58	GOLD	38011	37511
59	SILVER	5882	5382
60	BRONZE	5516	5016
61	GOLD	98464	97964
62	SILVER	27253	26753
63	BRONZE	79322	78822
64	GOLD	2205	1705
65	SILVER	44851	44351
66	BRONZE	26631	26131
67	GOLD	93220	92720
68	SILVER	19923	19423
69	BRONZE	6198	5698
70	GOLD	94151	93651
71	SILVER	66639	66139
72	BRONZE	82481	81981
73	GOLD	55406	54906
74	SILVER	5234	4734
75	BRONZE	5193	4693
76	GOLD	88839	88339
77	SILVER	11124	10624
78	BRONZE	83371	82871
79	GOLD	5323	4823
80	SILVER	20844	20344
81	BRONZE	23602	23102
82	GOLD	90774	90274
83	SILVER	75592	75092
84	BRONZE	11661	11161
85	GOLD	11010	10510
86	SILVER	16799	16299
87	BRONZE	65000	64500
88	GOLD	8980	8480
89	SILVER	36231	35731
90	BRONZE	2690	2190
91	GOLD	75524	75024
92	SILVER	56971	56471
93	BRONZE	86107	85607
94	GOLD	93638	93138
95	SILVER	73778	73278
96	BRONZE	4942	4442
97	GOLD	94588	94088
98	SILVER	38438	37938
99	BRONZE	42566	42066
100	GOLD	63776	63276
1	SILVER	76768	76268
2	BRONZE	94125	93625
3	GOLD	28518	28018
4	SILVER	20297	19797
5	BRONZE	76052	75552
6	GOLD	25179	24679
7	SILVER	82589	82089
8	BRONZE	71932	71432
9	GOLD	46087	45587
10	SILVER	1344	844
11	BRONZE	1716	1216
12	GOLD	82557	82057
13	SILVER	94120	93620
14	BRONZE	97224	96724
15	GOLD	4729	4229
16	SILVER	31454	30954
17	BRONZE	32331	31831
18	GOLD	73412	72912
19	SILVER	76676	76176
20	BRONZE	45250	44750
21	GOLD	28512	28012
22	SILVER	97917	97417
23	BRONZE	13794	13294
24	GOLD	88072	87572
25	SILVER	56563	56063
26	BRONZE	81465	80965
27	GOLD	81179	80679
28	SILVER	33405	32905
29	BRONZE	94678	94178
30	GOLD	71405	70905
31	SILVER	35770	35270
32	BRONZE	43003	42503
33	GOLD	78420	77920
34	SILVER	28274	27774
35	BRONZE	85411	84911
36	GOLD	78621	78121
37	SILVER	52747	52247
38	BRONZE	41843	41343
39	GOLD	95314	94814
40	SILVER	3849	3349
41	BRONZE	14278	13778
42	GOLD	28483	27983
43	SILVER	34101	33601
44	BRONZE	79555	79055
45	GOLD	45120	44620
46	SILVER	66490	65990
47	BRONZE	54450	53950
48	GOLD	55650	55150
49	SILVER	70025	69525
50	BRONZE	1018	518
51	GOLD	2477	1977
52	SILVER	8301	7801
53	BRONZE	57896	57396
54	GOLD	82673	82173
55	SILVER	66344	65844
56	BRONZE	1611	1111
57	GOLD	77195	76695
58	SILVER	35744	35244
59	BRONZE	29109	28609
60	GOLD	93619	93119
61	SILVER	39700	39200
62	BRONZE	15962	15462
63	GOLD	90984	90484
64	SILVER	89459	88959
65	BRONZE	31579	31079
66	GOLD	85351	84851
67	SILVER	88255	87755
68	BRONZE	62113	61613
69	GOLD	75252	74752
70	SILVER	36225	35725
71	BRONZE	90355	89855
72	GOLD	64108	63608
73	SILVER	49823	49323
74	BRONZE	51266	50766
75	GOLD	70787	70287
76	SILVER	8766	8266
77	BRONZE	11808	11308
78	GOLD	19614	19114
79	SILVER	21376	20876
80	BRONZE	55661	55161
81	GOLD	5365	4865
82	SILVER	65784	65284
83	BRONZE	27912	27412
84	GOLD	83962	83462
85	SILVER	22518	22018
86	BRONZE	79452	78952
87	GOLD	13754	13254
88	SILVER	52027	51527
89	BRONZE	62120	61620
90	GOLD	54239	53739
91	SILVER	95588	95088
92	BRONZE	52936	52436
93	GOLD	8321	7821
94	SILVER	35762	35262
95	BRONZE	59965	59465
96	GOLD	32964	32464
97	SILVER	17222	16722
98	BRONZE	96787	96287
99	GOLD	68427	67927
100	SILVER	50251	49751
1	BRONZE	97966	97466
2	GOLD	17676	17176
3	SILVER	57765	57265
4	BRONZE	72497	71997
5	GOLD	32224	31724
6	SILVER	25009	24509
7	BRONZE	52354	51854
8	GOLD	70040	69540
9	SILVER	36921	36421
10	BRONZE	40293	39793
11	GOLD	27416	26916
12	SILVER	73280	72780
13	BRONZE	50341	49841
14	GOLD	12363	11863
15	SILVER	14519	14019
16	BRONZE	69102	68602
17	GOLD	46098	45598
18	SILVER	30177	29677
19	BRONZE	85580	85080
20	GOLD	13885	13385
21	SILVER	92031	91531
22	BRONZE	55828	55328
23	GOLD	89289	88789
24	SILVER	78393	77893
25	BRONZE	3651	3151
26	GOLD	66289	65789
27	SILVER	26622	26122
28	BRONZE	56191	55691
29	GOLD	53222	52722
30	SILVER	41117	40617
31	BRONZE	45767	45267
32	GOLD	39268	38768
33	SILVER	25848	25348
34	BRONZE	9210	8710
35	GOLD	81368	80868
36	SILVER	77128	76628
37	BRONZE	64010	63510
38	GOLD	48084	47584
39	SILVER	96408	95908
40	BRONZE	20695	20195
41	GOLD	99125	98625
42	SILVER	15732	15232
43	BRONZE	35452	34952
44	GOLD	46511	46011
45	SILVER	98388	97888
46	BRONZE	24758	24258
47	GOLD	83620	83120
48	SILVER	8393	7893
49	BRONZE	34343	33843
50	GOLD	77268	76768
51	SILVER	92983	92483
52	BRONZE	64239	63739
53	GOLD	98531	98031
54	SILVER	29341	28841
55	BRONZE	92967	92467
56	GOLD	41648	41148
57	SILVER	25159	24659
58	BRONZE	61961	61461
59	GOLD	90072	89572
60	SILVER	29762	29262
61	BRONZE	80703	80203
62	GOLD	55503	55003
63	SILVER	54390	53890
64	BRONZE	34456	33956
65	GOLD	21543	21043
66	SILVER	44339	43839
67	BRONZE	86595	86095
68	GOLD	60126	59626
69	SILVER	71412	70912
70	BRONZE	26477	25977
71	GOLD	27100	26600
72	SILVER	71882	71382
73	BRONZE	17777	17277
74	GOLD	27779	27279
75	SILVER	8800	8300
76	BRONZE	66958	66458
77	GOLD	71188	70688
78	SILVER	54435	53935
79	BRONZE	36773	36273
80	GOLD	68215	67715
81	SILVER	78630	78130
82	BRONZE	56268	55768
83	GOLD	70950	70450
84	SILVER	82703	82203
85	BRONZE	12697	12197
86	GOLD	25068	24568
87	SILVER	21621	21121
88	BRONZE	91984	91484
89	GOLD	13331	12831
90	SILVER	48850	48350
91	BRONZE	39559	39059
92	GOLD	62529	62029
93	SILVER	9359	8859
94	BRONZE	6976	6476
95	GOLD	74640	74140
96	SILVER	75650	75150
97	BRONZE	96278	95778
98	GOLD	19383	18883
99	SILVER	46605	46105
100	BRONZE	84139	83639
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.item (nama, deskripsi, usia_dari, usia_sampai, bahan) FROM stdin;
lorem. Donec elementum, lorem	\N	1	4	sapien, gravida
sit amet, risus.	\N	1	7	purus, accumsan
et, commodo at, libero.	\N	1	8	augue id
a sollicitudin orci	\N	1	7	Cras sed
sapien imperdiet ornare. In	\N	1	8	ornare, lectus
lacus. Cras	\N	1	4	aliquam iaculis,
per conubia nostra, per	\N	1	6	non sapien
facilisis vitae, orci. Phasellus	\N	1	17	molestie orci
mollis	\N	1	6	non enim
molestie tellus. Aenean egestas	\N	1	4	lacus. Ut
eu	\N	2	12	dui, semper
risus.	\N	2	11	malesuada fames
quis diam luctus lobortis. Class	\N	2	5	augue malesuada
vel, vulputate eu, odio. Phasellus	\N	2	11	velit. Sed
luctus ut, pellentesque	\N	2	9	Fusce fermentum
at auctor ullamcorper, nisl arcu	\N	3	5	taciti sociosqu
metus	\N	3	13	Aliquam ultrices
mus. Proin	\N	3	9	Phasellus fermentum
dapibus rutrum, justo. Praesent luctus.	\N	3	9	ultrices posuere
enim.	\N	3	13	luctus. Curabitur
nunc	\N	4	13	bibendum. Donec
tellus	\N	4	8	neque pellentesque
gravida. Praesent eu	\N	4	6	ultrices. Vivamus
neque tellus,	\N	4	9	arcu. Vestibulum
vulputate	\N	4	13	purus. Duis
\.


--
-- Data for Name: kategori; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.kategori (nama, level, sub_dari) FROM stdin;
Mainan Motorik	1	Mainan Berwarna
Mainan Berwarna	2	Mainan Motorik
Mainan Rumah	2	Mainan Besar
Mainan Besar	3	Mainan Rumah
Mainan Pantai	1	Mainan Besar
\.


--
-- Data for Name: kategori_item; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.kategori_item (nama_item, nama_kategori) FROM stdin;
lorem. Donec elementum, lorem	Mainan Motorik
sit amet, risus.	Mainan Motorik
et, commodo at, libero.	Mainan Motorik
a sollicitudin orci	Mainan Motorik
sapien imperdiet ornare. In	Mainan Motorik
lacus. Cras	Mainan Motorik
per conubia nostra, per	Mainan Motorik
facilisis vitae, orci. Phasellus	Mainan Motorik
mollis	Mainan Motorik
molestie tellus. Aenean egestas	Mainan Motorik
eu	Mainan Motorik
risus.	Mainan Motorik
quis diam luctus lobortis. Class	Mainan Motorik
vel, vulputate eu, odio. Phasellus	Mainan Motorik
luctus ut, pellentesque	Mainan Motorik
at auctor ullamcorper, nisl arcu	Mainan Berwarna
metus	Mainan Berwarna
mus. Proin	Mainan Berwarna
dapibus rutrum, justo. Praesent luctus.	Mainan Berwarna
enim.	Mainan Berwarna
nunc	Mainan Berwarna
tellus	Mainan Berwarna
gravida. Praesent eu	Mainan Berwarna
neque tellus,	Mainan Berwarna
vulputate	Mainan Berwarna
lorem. Donec elementum, lorem	Mainan Berwarna
sit amet, risus.	Mainan Berwarna
et, commodo at, libero.	Mainan Berwarna
a sollicitudin orci	Mainan Berwarna
sapien imperdiet ornare. In	Mainan Berwarna
lacus. Cras	Mainan Rumah
per conubia nostra, per	Mainan Rumah
facilisis vitae, orci. Phasellus	Mainan Rumah
mollis	Mainan Rumah
molestie tellus. Aenean egestas	Mainan Rumah
eu	Mainan Rumah
risus.	Mainan Rumah
quis diam luctus lobortis. Class	Mainan Rumah
vel, vulputate eu, odio. Phasellus	Mainan Rumah
luctus ut, pellentesque	Mainan Rumah
at auctor ullamcorper, nisl arcu	Mainan Rumah
metus	Mainan Rumah
mus. Proin	Mainan Rumah
dapibus rutrum, justo. Praesent luctus.	Mainan Rumah
enim.	Mainan Rumah
nunc	Mainan Besar
tellus	Mainan Besar
gravida. Praesent eu	Mainan Besar
neque tellus,	Mainan Besar
vulputate	Mainan Besar
lorem. Donec elementum, lorem	Mainan Besar
sit amet, risus.	Mainan Besar
et, commodo at, libero.	Mainan Besar
a sollicitudin orci	Mainan Besar
sapien imperdiet ornare. In	Mainan Besar
lacus. Cras	Mainan Besar
per conubia nostra, per	Mainan Besar
facilisis vitae, orci. Phasellus	Mainan Besar
mollis	Mainan Besar
molestie tellus. Aenean egestas	Mainan Besar
eu	Mainan Pantai
risus.	Mainan Pantai
quis diam luctus lobortis. Class	Mainan Pantai
vel, vulputate eu, odio. Phasellus	Mainan Pantai
luctus ut, pellentesque	Mainan Pantai
at auctor ullamcorper, nisl arcu	Mainan Pantai
metus	Mainan Pantai
mus. Proin	Mainan Pantai
dapibus rutrum, justo. Praesent luctus.	Mainan Pantai
enim.	Mainan Pantai
nunc	Mainan Pantai
tellus	Mainan Pantai
gravida. Praesent eu	Mainan Pantai
neque tellus,	Mainan Pantai
vulputate	Mainan Pantai
\.


--
-- Data for Name: level_keanggotaan; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.level_keanggotaan (nama_level, minimum_poin, deskripsi) FROM stdin;
GOLD	800	Level 1
SILVER	300	Level 2
BRONZE	0	Level 3
\.


--
-- Data for Name: pemesanan; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.pemesanan (id_pemesanan, datetime_pesanan, kuantitas_barang, harga_sewa, ongkos, no_ktp_pemesan, status) FROM stdin;
4358786618	2018-09-03 00:00:00	6	577168	77381	1665050425199	SEDANG DIKONFIRMASI
3914039361	2018-12-24 00:00:00	7	635753	2555	1660031233199	SEDANG DISIAPKAN
4439622607	2018-12-16 00:00:00	7	371099	45135	1652031313299	SEDANG DIKIRIM
6287741309	2019-01-09 00:00:00	4	171208	54620	1696030740299	DALAM MASA SEWA
9150399314	2018-10-13 00:00:00	2	217189	10090	1684012162899	SUDAH DIKEMBALIKAN
1722807687	2018-02-22 00:00:00	1	826054	62647	1616092707799	SUDAH DIKEMBALIKAN
3886354008	2018-06-25 00:00:00	1	359585	38869	1666111561499	SEDANG DIKONFIRMASI
4893997866	2019-01-30 00:00:00	5	480099	74688	1654091365799	SEDANG DISIAPKAN
2716502501	2018-02-20 00:00:00	5	339573	96341	1690080707099	SEDANG DIKIRIM
2956957813	2018-01-20 00:00:00	6	876155	85421	1647071222999	DALAM MASA SEWA
9034274004	2018-04-20 00:00:00	1	547705	71503	1682091892499	SUDAH DIKEMBALIKAN
3988911615	2018-04-04 00:00:00	6	347847	50290	1655022813899	SUDAH DIKEMBALIKAN
5363235951	2018-06-18 00:00:00	5	741481	20309	1626111281199	SEDANG DIKONFIRMASI
5149585726	2018-03-04 00:00:00	9	394416	28571	1619100330199	SEDANG DISIAPKAN
2713578175	2018-08-22 00:00:00	0	819986	16464	1618081701399	SEDANG DIKIRIM
6782050293	2018-12-30 00:00:00	8	213218	8438	1653010247599	DALAM MASA SEWA
9347267287	2018-04-04 00:00:00	6	143685	73726	1692113095999	SUDAH DIKEMBALIKAN
2023033586	2018-02-11 00:00:00	2	874243	88761	1677050256499	SUDAH DIKEMBALIKAN
2881534376	2018-11-17 00:00:00	8	528170	42279	1675070211499	SEDANG DIKONFIRMASI
3837372537	2018-01-24 00:00:00	2	602646	78252	1664050390599	SEDANG DISIAPKAN
9006328618	2018-02-18 00:00:00	6	568602	35124	1638071836799	SEDANG DIKIRIM
988293536	2018-03-23 00:00:00	4	50221	52833	1607051844399	DALAM MASA SEWA
378785931	2018-03-03 00:00:00	4	215880	94697	1606091595499	SUDAH DIKEMBALIKAN
6717097140	2018-11-09 00:00:00	4	261170	41139	1638082087499	SUDAH DIKEMBALIKAN
523614551	2019-01-04 00:00:00	9	770198	5470	1642050910199	SEDANG DIKONFIRMASI
3396141148	2018-10-06 00:00:00	8	816286	26067	1631022299499	SEDANG DISIAPKAN
1249492157	2018-07-18 00:00:00	4	547963	92046	1689092362299	SEDANG DIKIRIM
2519904208	2018-05-30 00:00:00	1	9300	43644	1689021680899	DALAM MASA SEWA
8730457513	2018-05-10 00:00:00	1	908952	27321	1698120234499	SUDAH DIKEMBALIKAN
9222573471	2018-05-06 00:00:00	0	14542	78166	1615011935599	SUDAH DIKEMBALIKAN
7076376695	2018-12-14 00:00:00	8	910364	21833	1646111317399	SEDANG DIKONFIRMASI
236916904	2018-01-16 00:00:00	10	591237	5817	1641051558699	SEDANG DISIAPKAN
332962296	2018-10-06 00:00:00	8	999403	27531	1615090330999	SEDANG DIKIRIM
4099390772	2018-08-25 00:00:00	4	191563	59432	1661021923199	DALAM MASA SEWA
6869799660	2018-04-28 00:00:00	6	108899	21486	1604080162599	SUDAH DIKEMBALIKAN
5195025134	2018-06-27 00:00:00	9	427329	75960	1670120850299	SUDAH DIKEMBALIKAN
6379737650	2018-08-22 00:00:00	5	426766	40397	1609032569999	SEDANG DIKONFIRMASI
9873927840	2018-08-26 00:00:00	9	383272	18277	1641110636299	SEDANG DISIAPKAN
1536799653	2018-07-19 00:00:00	8	969803	63752	1683031927399	SEDANG DIKIRIM
6602767755	2018-03-11 00:00:00	5	943348	17939	1667041880599	DALAM MASA SEWA
9525456463	2018-12-21 00:00:00	8	75413	33914	1620101983699	SUDAH DIKEMBALIKAN
3569575977	2019-01-07 00:00:00	10	461638	38140	1639122775999	SUDAH DIKEMBALIKAN
6264163384	2018-09-21 00:00:00	2	34601	5495	1671121326299	SEDANG DIKONFIRMASI
9076717699	2018-05-06 00:00:00	7	919084	32988	1641101587799	SEDANG DISIAPKAN
3562680244	2018-03-11 00:00:00	4	287276	15071	1601092899599	SEDANG DIKIRIM
5678778056	2018-05-12 00:00:00	10	592130	14744	1603080783099	DALAM MASA SEWA
652997198	2018-07-04 00:00:00	5	769599	56424	1678012023599	SUDAH DIKEMBALIKAN
4779757363	2018-06-28 00:00:00	4	617864	29313	1697062613399	SUDAH DIKEMBALIKAN
7282724776	2018-07-16 00:00:00	1	531764	32133	1647011107599	SEDANG DIKONFIRMASI
1009692399	2018-01-23 00:00:00	4	53369	22239	1694050773099	SEDANG DISIAPKAN
\.


--
-- Data for Name: pengembalian; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.pengembalian (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
VPN8883379	4358786618	TARO GUDANG	77381	2019-12-22	1665050425199	Mockingbird
LPU6571362	3914039361	DIJEMPUT	2555	2019-12-07	1660031233199	Melvin
JCL2238630	4439622607	TARO GUDANG	45135	2019-11-14	1652031313299	Michigan
QOJ7774351	6287741309	DIJEMPUT	54620	2019-12-17	1696030740299	Banding
MPD1748146	9150399314	TARO GUDANG	10090	2019-12-20	1684012162899	Moulton
JWP5352231	1722807687	DIJEMPUT	62647	2019-11-27	1616092707799	Lawn
SXN8470256	3886354008	TARO GUDANG	38869	2019-11-14	1666111561499	Briar Crest
MHZ2398148	4893997866	DIJEMPUT	74688	2019-11-20	1654091365799	Oak Valley
HKP2095058	2716502501	TARO GUDANG	96341	2019-12-12	1690080707099	Merchant
NKH8313913	2956957813	DIJEMPUT	85421	2019-11-30	1647071222999	Cottonwood
XOZ6930390	9034274004	TARO GUDANG	71503	2019-12-10	1682091892499	Charing Cross
UMB3513497	3988911615	DIJEMPUT	50290	2019-12-11	1655022813899	Marcy
RUK2602423	5363235951	TARO GUDANG	20309	2019-12-17	1626111281199	Buell
QYM7492758	5149585726	DIJEMPUT	28571	2019-11-04	1619100330199	Summit
EZG0726229	2713578175	TARO GUDANG	16464	2019-11-26	1618081701399	Welch
SYJ4042072	6782050293	DIJEMPUT	8438	2019-11-14	1653010247599	Waxwing
QAB2182681	9347267287	TARO GUDANG	73726	2019-12-09	1692113095999	Grasskamp
EHJ6424027	2023033586	DIJEMPUT	88761	2019-12-17	1677050256499	Grayhawk
VGQ1606101	2881534376	TARO GUDANG	42279	2019-11-25	1675070211499	Tony
FAY9240972	3837372537	DIJEMPUT	78252	2019-12-07	1664050390599	Sherman
FAY9240973	9006328618	TARO GUDANG	78252	2019-12-05	1638071836799	Hansons
FAY9240974	988293536	DIJEMPUT	78252	2019-11-19	1607051844399	Buell
FAY9240975	378785931	TARO GUDANG	78252	2019-11-15	1606091595499	Twin Pines
FAY9240976	6717097140	DIJEMPUT	78252	2019-11-01	1638082087499	International
FAY9240977	523614551	TARO GUDANG	78252	2019-12-22	1642050910199	Sullivan
FAY9240978	3396141148	DIJEMPUT	78252	2019-12-01	1631022299499	Thackeray
FAY9240979	1249492157	TARO GUDANG	78252	2019-12-02	1689092362299	Superior
FAY9240980	2519904208	DIJEMPUT	78252	2019-12-05	1689021680899	Tennessee
FAY9240981	8730457513	TARO GUDANG	78252	2019-11-05	1698120234499	Beilfuss
FAY9240982	9222573471	DIJEMPUT	78252	2019-12-01	1615011935599	Acker
FAY9240983	7076376695	TARO GUDANG	78252	2019-12-28	1646111317399	Arizona
FAY9240984	236916904	DIJEMPUT	78252	2019-12-09	1641051558699	American
FAY9240985	332962296	TARO GUDANG	78252	2019-11-01	1615090330999	South
FAY9240986	4099390772	DIJEMPUT	78252	2019-11-08	1661021923199	Hagan
FAY9240987	6869799660	TARO GUDANG	78252	2019-11-06	1604080162599	Jackson
FAY9240988	5195025134	DIJEMPUT	78252	2019-12-08	1670120850299	Bultman
FAY9240989	6379737650	TARO GUDANG	78252	2019-11-22	1609032569999	Marcy
FAY9240990	9873927840	DIJEMPUT	78252	2019-12-02	1641110636299	Almo
FAY9240991	1536799653	TARO GUDANG	78252	2019-11-07	1683031927399	Anderson
FAY9240992	6602767755	DIJEMPUT	78252	2019-12-11	1667041880599	Hanson
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.pengguna (no_ktp, nama_lengkap, email, tanggal_lahir, no_telp) FROM stdin;
1665050425199	Hall Dejesus	aliquam.iaculis@ornarelectus.ca	1931-01-17	016-604-0569
1660031233199	Bert Bass	pretium@rutrumnonhendrerit.ca	1957-10-22	098-632-9314
1652031313299	Kuame Castillo	adipiscing@nasceturridiculusmus.ca	1892-09-20	041-462-2232
1696030740299	Hall Peters	massa.rutrum.magna@ridiculusmusAenean.org	1918-11-11	069-356-0428
1684012162899	Shad Glover	eleifend.non@ipsumSuspendisse.net	1914-07-25	006-841-3127
1616092707799	Lewis Vasquez	pellentesque@enimMauris.com	1949-12-24	066-981-1225
1666111561499	Aquila Buchanan	malesuada.fringilla@metusurnaconvallis.edu	1921-09-08	097-935-2874
1654091365799	Mannix Collins	magna.Cras.convallis@laciniaSed.co.uk	1906-07-22	011-604-0570
1690080707099	Burton Acosta	ullamcorper@odioNam.org	1896-02-04	072-088-1843
1647071222999	Salvador Sloan	Nunc.mauris@necanteMaecenas.net	1884-11-15	075-020-7952
1682091892499	Hyatt Duncan	nonummy.ac@dictumeu.org	1973-05-14	074-472-4881
1655022813899	Knox Cohen	lacus.Cras@Nunclectus.com	1970-10-15	026-485-9348
1626111281199	Colby Conrad	sed.dictum.eleifend@cursus.co.uk	1929-08-21	012-360-5703
1619100330199	Kirk Carrillo	mauris.ipsum.porta@molestiedapibusligula.ca	1953-08-04	039-510-7927
1618081701399	Lee Murray	Vivamus@diam.edu	1882-01-23	073-321-1860
1653010247599	Ralph Roman	odio.tristique@egestas.com	1903-05-29	026-406-8358
1692113095999	Davis Grant	metus.Aliquam.erat@et.org	1873-12-01	092-983-5060
1677050256499	Eaton Kelly	primis.in.faucibus@milorem.ca	1882-09-12	045-045-9138
1675070211499	Garth Travis	tempor@Aliquamerat.com	1905-10-22	013-500-3849
1664050390599	Giacomo Reid	vehicula.risus@augueacipsum.co.uk	1935-03-20	048-976-0584
1638071836799	Caesar Kent	eu.ligula@ornare.edu	1931-02-15	033-353-7530
1607051844399	Benjamin Brady	nulla.ante@ornare.com	1880-04-24	042-549-0806
1606091595499	Joshua York	lorem.auctor.quis@magnaNamligula.edu	1883-07-09	070-162-9506
1638082087499	Stewart Stevens	a@lacinia.org	1892-01-27	022-671-2798
1642050910199	Alvin Fuller	lacus@euismod.edu	1925-12-14	029-389-0636
1631022299499	Zeus Rhodes	Nullam.feugiat@leoVivamus.org	1987-01-26	085-729-1679
1689092362299	Timothy Ferguson	eget.metus@ultricesDuisvolutpat.co.uk	1980-08-27	041-460-9037
1689021680899	Tate Berry	Donec@mollisDuissit.co.uk	1955-05-20	072-922-8817
1698120234499	August Holt	non.luctus.sit@mus.co.uk	1988-07-05	038-239-1331
1615011935599	Ronan Reid	arcu.Sed.et@aliquam.ca	1905-01-13	063-094-7009
1646111317399	Jerry Hubbard	lorem.sit.amet@aneque.ca	1900-10-13	059-154-1112
1641051558699	Lucius Wallace	elit.pede.malesuada@dictum.edu	1893-05-19	078-090-1869
1615090330999	Stephen Roy	amet.massa.Quisque@et.com	1965-03-12	033-916-3355
1661021923199	Caldwell Collins	ac.turpis@Donecest.org	1917-12-19	070-192-0712
1604080162599	William Cote	at.egestas.a@feugiat.co.uk	1975-05-25	083-998-3034
1670120850299	Yoshio Bond	sem@lectusjusto.co.uk	1969-04-21	029-578-8537
1609032569999	Emerson Walter	habitant.morbi.tristique@luctus.edu	1905-01-23	039-294-9241
1641110636299	Abbot Parks	at@fringilla.edu	1988-06-11	064-943-7534
1683031927399	Magee Ballard	consectetuer.mauris.id@feugiatnec.net	1970-01-21	052-417-5461
1667041880599	Thomas Blevins	dolor.elit@nonummy.edu	1972-09-27	007-262-6294
1620101983699	Hashim Barber	lobortis.ultrices@Nullatempor.com	1931-01-17	098-104-2074
1639122775999	Knox Soto	convallis@Donec.net	1957-10-22	015-793-6077
1671121326299	Chandler Case	fringilla@natoquepenatibuset.edu	1892-09-20	006-825-6196
1641101587799	Walter Benjamin	erat@egestasascelerisque.org	1918-11-11	014-732-0082
1601092899599	Forrest Moon	malesuada@sitamet.edu	1914-07-25	026-082-8420
1603080783099	Kenyon Mayo	nec@liberomaurisaliquam.co.uk	1949-12-24	082-296-4769
1678012023599	Giacomo Gilliam	facilisis@Donecluctus.com	1921-09-08	029-827-0248
1697062613399	Chase Grimes	ante.Maecenas.mi@Sednec.com	1906-07-22	058-534-7962
1647011107599	Aaron Robbins	lectus.rutrum@quamquisdiam.net	1896-02-04	024-163-3763
1694050773099	Fuller Alford	neque.venenatis@Aliquamadipiscing.net	1884-11-15	081-013-9990
1614081344199	Timothy Marquez	Nunc@ridiculus.edu	1973-05-14	047-056-0960
1674121319999	Preston Skinner	eget.lacus.Mauris@Proineget.com	1970-10-15	089-136-7429
1621041858199	Ian Chavez	egestas.hendrerit@musProin.org	1929-08-21	000-460-9145
1663102118499	Akeem Mckinney	elit.dictum.eu@laoreetposuere.ca	1953-08-04	029-966-6222
1614103053699	Kieran Rice	consectetuer@tinciduntnunc.org	1882-01-23	024-873-6718
1610020899899	Daquan Irwin	nisi.dictum@mauris.edu	1903-05-29	059-826-2041
1611052299599	Troy Cervantes	consequat.enim@sit.com	1873-12-01	085-175-8090
1676040595999	Deacon Lawrence	Donec.egestas@sapienCrasdolor.ca	1882-09-12	027-596-2020
1649061934799	Zeph Sanford	velit.in.aliquet@velitSed.com	1905-10-22	093-114-1777
1639040163699	Timon Cooley	enim.condimentum@Etiam.org	1935-03-20	089-418-0978
1600110143899	Luke Davidson	mi.felis@placerataugueSed.co.uk	1931-02-15	013-448-1033
1638100504299	Damian Brown	Nunc.mauris@pharetraQuisqueac.edu	1880-04-24	094-378-6056
1692021314399	Thaddeus Gonzalez	pharetra.nibh@nulla.net	1883-07-09	035-602-0447
1663080662499	Eaton Bryan	scelerisque.mollis.Phasellus@dictum.org	1892-01-27	044-468-5062
1636060497299	Ignatius Bush	ante.lectus@at.co.uk	1925-12-14	017-951-5350
1607042618599	Elmo Pope	Etiam@nec.org	1987-01-26	036-028-4205
1689051327599	Ryder Woods	fringilla@lacusQuisqueimperdiet.co.uk	1980-08-27	009-612-4008
1686072318499	Chase Forbes	Sed@enimcondimentumeget.org	1955-05-20	000-903-2226
1658040760699	Judah Keith	mi@velconvallisin.org	1988-07-05	007-263-8137
1670112789499	Reuben Harrell	Proin.non@ridiculus.edu	1905-01-13	022-210-9578
1641062533799	Paki Lyons	dictum.mi@nequevitae.edu	1900-10-13	055-566-3710
1628021887599	Owen Ellison	adipiscing@utaliquamiaculis.ca	1893-05-19	051-805-1767
1629041241299	Beau Hays	sodales.nisi@nonquamPellentesque.org	1965-03-12	089-891-9673
1682082408099	Jin Foster	vulputate.dui.nec@Quisque.org	1917-12-19	037-982-9720
1652102354499	Forrest Spencer	sed.leo.Cras@arcuVivamus.co.uk	1975-05-25	026-351-7261
1671122072799	Lewis Wiley	lacinia.at.iaculis@euismodac.com	1969-04-21	006-290-8995
1625052383799	Moses Floyd	ante@gravidaPraesent.com	1905-01-23	088-864-7347
1614112539899	Malcolm Craig	risus@molestie.com	1988-06-11	052-696-4062
1615050466499	Seth Castillo	sed.orci.lobortis@metusAeneansed.ca	1970-01-21	095-547-1347
1684091449699	Christopher Nguyen	ornare.egestas.ligula@interdum.co.uk	1972-09-27	045-498-4782
1657052470299	Jackson Meadows	consequat@vulputateveliteu.org	1931-01-17	059-977-2865
1615030986699	Clayton Hudson	hymenaeos.Mauris@estNunc.com	1957-10-22	091-014-2283
1617032381299	Dale Sawyer	Nullam.ut@rutrumFuscedolor.net	1892-09-20	011-275-8833
1631052448399	Leo Allen	Vestibulum.accumsan@pharetra.net	1918-11-11	097-448-6972
1618011334599	Ishmael Sweet	Praesent@eu.ca	1914-07-25	077-099-1143
1602070815399	Arden Gomez	urna.convallis.erat@NulladignissimMaecenas.ca	1949-12-24	083-870-5961
1629112962199	Lamar Cohen	lobortis@libero.net	1921-09-08	079-077-6733
1672080288399	Noble Jarvis	Nunc.pulvinar@tellusjusto.co.uk	1906-07-22	070-600-3662
1660090147199	Acton Stewart	nisl.arcu@aliquetnec.co.uk	1896-02-04	048-929-5712
1661052642199	Roth Mack	Suspendisse@urnaVivamus.co.uk	1884-11-15	018-294-3274
1685012399499	Damon Schneider	magna.a.tortor@libero.co.uk	1973-05-14	022-442-3257
1695111896499	Xavier Cardenas	convallis@ornarelibero.ca	1970-10-15	059-451-4507
1651100890699	Duncan Chandler	eu@semperetlacinia.ca	1929-08-21	083-813-6566
1660082579299	Kennan Lewis	Fusce@dignissimpharetraNam.org	1953-08-04	084-874-0686
1690070372999	Oleg Walker	auctor@posuereenimnisl.com	1882-01-23	046-834-6880
1607032122799	Dante Ochoa	non@nislNullaeu.net	1903-05-29	090-910-4165
1620100974199	Melvin Ruiz	a.neque@nisi.com	1873-12-01	098-544-6487
1684121769799	Talon Middleton	pharetra.Quisque@ipsumcursus.net	1882-09-12	050-345-6331
1605111218599	Aladdin Rodriquez	luctus@acorciUt.ca	1905-10-22	085-082-0850
1647071347299	Ralph Jackson	sapien.cursus.in@liberoProinsed.co.uk	1935-03-20	075-371-0607
\.


--
-- Data for Name: pengiriman; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.pengiriman (no_resi, id_pemesanan, metode, ongkos, tanggal, no_ktp_anggota, nama_alamat_anggota) FROM stdin;
VPN8883379	4358786618	AMBIL KE GUDANG	77381	2019-05-10	1665050425199	Mockingbird
LPU6571362	3914039361	KIRIM	2555	2019-07-24	1660031233199	Melvin
JCL2238630	4439622607	AMBIL KE GUDANG	45135	2019-05-07	1652031313299	Michigan
QOJ7774351	6287741309	KIRIM	54620	2019-06-09	1696030740299	Banding
MPD1748146	9150399314	AMBIL KE GUDANG	10090	2019-05-13	1684012162899	Moulton
JWP5352231	1722807687	KIRIM	62647	2019-07-14	1616092707799	Lawn
SXN8470256	3886354008	AMBIL KE GUDANG	38869	2019-07-23	1666111561499	Briar Crest
MHZ2398148	4893997866	KIRIM	74688	2019-07-23	1654091365799	Oak Valley
HKP2095058	2716502501	AMBIL KE GUDANG	96341	2019-05-23	1690080707099	Merchant
NKH8313913	2956957813	KIRIM	85421	2019-06-22	1647071222999	Cottonwood
XOZ6930390	9034274004	AMBIL KE GUDANG	71503	2019-07-06	1682091892499	Charing Cross
UMB3513497	3988911615	KIRIM	50290	2019-06-13	1655022813899	Marcy
RUK2602423	5363235951	AMBIL KE GUDANG	20309	2019-06-12	1626111281199	Buell
QYM7492758	5149585726	KIRIM	28571	2019-05-16	1619100330199	Summit
EZG0726229	2713578175	AMBIL KE GUDANG	16464	2019-05-20	1618081701399	Welch
SYJ4042072	6782050293	KIRIM	8438	2019-05-17	1653010247599	Waxwing
QAB2182681	9347267287	AMBIL KE GUDANG	73726	2019-05-14	1692113095999	Grasskamp
EHJ6424027	2023033586	KIRIM	88761	2019-06-02	1677050256499	Grayhawk
VGQ1606101	2881534376	AMBIL KE GUDANG	42279	2019-06-12	1675070211499	Tony
FAY9240972	3837372537	KIRIM	78252	2019-06-17	1664050390599	Sherman
FAY9240973	9006328618	AMBIL KE GUDANG	78252	2019-07-07	1638071836799	Hansons
FAY9240974	988293536	KIRIM	78252	2019-06-10	1607051844399	Buell
FAY9240975	378785931	AMBIL KE GUDANG	78252	2019-05-16	1606091595499	Twin Pines
FAY9240976	6717097140	KIRIM	78252	2019-07-17	1638082087499	International
FAY9240977	523614551	AMBIL KE GUDANG	78252	2019-07-04	1642050910199	Sullivan
FAY9240978	3396141148	KIRIM	78252	2019-06-09	1631022299499	Thackeray
FAY9240979	1249492157	AMBIL KE GUDANG	78252	2019-07-16	1689092362299	Superior
FAY9240980	2519904208	KIRIM	78252	2019-05-31	1689021680899	Tennessee
FAY9240981	8730457513	AMBIL KE GUDANG	78252	2019-06-01	1698120234499	Beilfuss
FAY9240982	9222573471	KIRIM	78252	2019-06-08	1615011935599	Acker
FAY9240983	7076376695	AMBIL KE GUDANG	78252	2019-05-11	1646111317399	Arizona
FAY9240984	236916904	KIRIM	78252	2019-07-10	1641051558699	American
FAY9240985	332962296	AMBIL KE GUDANG	78252	2019-05-30	1615090330999	South
FAY9240986	4099390772	KIRIM	78252	2019-07-20	1661021923199	Hagan
FAY9240987	6869799660	AMBIL KE GUDANG	78252	2019-05-01	1604080162599	Jackson
FAY9240988	5195025134	KIRIM	78252	2019-05-04	1670120850299	Bultman
FAY9240989	6379737650	AMBIL KE GUDANG	78252	2019-07-06	1609032569999	Marcy
FAY9240990	9873927840	KIRIM	78252	2019-07-10	1641110636299	Almo
FAY9240991	1536799653	AMBIL KE GUDANG	78252	2019-06-02	1683031927399	Anderson
FAY9240992	6602767755	KIRIM	78252	2019-07-22	1667041880599	Hanson
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: db2018008
--

COPY public.status (nama, deskripsi) FROM stdin;
SEDANG DIKONFIRMASI	Proses penyewaan sedang dikonfirmasi.
SEDANG DISIAPKAN	Barang sedang disiapkan untuk dikirim.
SEDANG DIKIRIM	Barang dalam proses pengiriman.
DALAM MASA SEWA	Barang sedang dalam masa sewa.
SUDAH DIKEMBALIKAN	Barang sudah dikembalikan oleh penyewa.
BATAL	Penyewaan barang dibatalkan.
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (no_ktp);


--
-- Name: alamat alamat_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.alamat
    ADD CONSTRAINT alamat_pkey PRIMARY KEY (no_ktp_anggota, nama);


--
-- Name: anggota anggota_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_pkey PRIMARY KEY (no_ktp);


--
-- Name: barang_dikembalikan barang_dikembalikan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_dikirim barang_dikirim_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_pkey PRIMARY KEY (no_resi, no_urut);


--
-- Name: barang_pesanan barang_pesanan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_pkey PRIMARY KEY (id_pemesanan, no_urut);


--
-- Name: barang barang_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_pkey PRIMARY KEY (id_barang);


--
-- Name: chat chat_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_pkey PRIMARY KEY (id);


--
-- Name: info_barang_level info_barang_level_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_pkey PRIMARY KEY (id_barang, nama_level);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (nama);


--
-- Name: kategori_item kategori_item_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_pkey PRIMARY KEY (nama_item, nama_kategori);


--
-- Name: kategori kategori_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_pkey PRIMARY KEY (nama);


--
-- Name: level_keanggotaan level_keanggotaan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.level_keanggotaan
    ADD CONSTRAINT level_keanggotaan_pkey PRIMARY KEY (nama_level);


--
-- Name: pemesanan pemesanan_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_pkey PRIMARY KEY (id_pemesanan);


--
-- Name: pengembalian pengembalian_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_pkey PRIMARY KEY (no_resi);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (no_ktp);


--
-- Name: pengiriman pengiriman_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_pkey PRIMARY KEY (no_resi);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (nama);


--
-- Name: anggota poin_anggota_changes; Type: TRIGGER; Schema: public; Owner: db2018008
--
DROP TRIGGER poin_anggota_changes on public.anggota;
CREATE TRIGGER poin_anggota_changes AFTER INSERT OR UPDATE OF poin ON public.anggota FOR EACH ROW EXECUTE PROCEDURE public.poin_anggota_changes();


--
-- Name: level_keanggotaan poin_level_keanggotaan_changes; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER poin_level_keanggotaan_changes AFTER INSERT OR UPDATE OF minimum_poin ON public.level_keanggotaan FOR EACH ROW EXECUTE PROCEDURE public.poin_level_keanggotaan_changes();


--
-- Name: barang_pesanan tambah_harga_sewa; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER tambah_harga_sewa AFTER INSERT ON public.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE public.tambah_harga_sewa();


--
-- Name: pengiriman tambah_ongkos; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER tambah_ongkos AFTER INSERT ON public.pengiriman FOR EACH ROW EXECUTE PROCEDURE public.tambah_ongkos();


--
-- Name: pemesanan tambah_poin; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER tambah_poin AFTER INSERT ON public.pemesanan FOR EACH ROW EXECUTE PROCEDURE public.update_poin();


--
-- Name: barang tambah_poin_penyewa; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER tambah_poin_penyewa AFTER INSERT ON public.barang FOR EACH ROW EXECUTE PROCEDURE public.update_poin_penyewa();


--
-- Name: barang_pesanan update_kolom_kondisi_barang_trigger; Type: TRIGGER; Schema: public; Owner: db2018008
--

CREATE TRIGGER update_kolom_kondisi_barang_trigger AFTER INSERT ON public.barang_pesanan FOR EACH ROW EXECUTE PROCEDURE public.update_kolom_kondisi_barang();


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES public.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin admin_no_ktp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES public.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat alamat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.alamat
    ADD CONSTRAINT alamat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_level_fkey FOREIGN KEY (level) REFERENCES public.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: anggota anggota_no_ktp_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.anggota
    ADD CONSTRAINT anggota_no_ktp_fkey FOREIGN KEY (no_ktp) REFERENCES public.pengguna(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikembalikan barang_dikembalikan_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikembalikan
    ADD CONSTRAINT barang_dikembalikan_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengembalian(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_dikirim barang_dikirim_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_dikirim
    ADD CONSTRAINT barang_dikirim_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_nama_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES public.item(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang barang_no_ktp_penyewa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang
    ADD CONSTRAINT barang_no_ktp_penyewa_fkey FOREIGN KEY (no_ktp_penyewa) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: barang_pesanan barang_pesanan_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.barang_pesanan
    ADD CONSTRAINT barang_pesanan_status_fkey FOREIGN KEY (status) REFERENCES public.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_admin_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_no_ktp_admin_fkey FOREIGN KEY (no_ktp_admin) REFERENCES public.admin(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: chat chat_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.chat
    ADD CONSTRAINT chat_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_id_barang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_id_barang_fkey FOREIGN KEY (id_barang) REFERENCES public.barang(id_barang) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: info_barang_level info_barang_level_nama_level_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.info_barang_level
    ADD CONSTRAINT info_barang_level_nama_level_fkey FOREIGN KEY (nama_level) REFERENCES public.level_keanggotaan(nama_level) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_item_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_nama_item_fkey FOREIGN KEY (nama_item) REFERENCES public.item(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_item kategori_item_nama_kategori_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.kategori_item
    ADD CONSTRAINT kategori_item_nama_kategori_fkey FOREIGN KEY (nama_kategori) REFERENCES public.kategori(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori kategori_sub_dari_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.kategori
    ADD CONSTRAINT kategori_sub_dari_fkey FOREIGN KEY (sub_dari) REFERENCES public.kategori(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_no_ktp_pemesan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_no_ktp_pemesan_fkey FOREIGN KEY (no_ktp_pemesan) REFERENCES public.anggota(no_ktp) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pemesanan pemesanan_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pemesanan
    ADD CONSTRAINT pemesanan_status_fkey FOREIGN KEY (status) REFERENCES public.status(nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES public.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengembalian pengembalian_no_resi_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengembalian
    ADD CONSTRAINT pengembalian_no_resi_fkey FOREIGN KEY (no_resi) REFERENCES public.pengiriman(no_resi) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_id_pemesanan_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_id_pemesanan_fkey FOREIGN KEY (id_pemesanan) REFERENCES public.pemesanan(id_pemesanan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengiriman pengiriman_no_ktp_anggota_fkey; Type: FK CONSTRAINT; Schema: public; Owner: db2018008
--

ALTER TABLE ONLY public.pengiriman
    ADD CONSTRAINT pengiriman_no_ktp_anggota_fkey FOREIGN KEY (no_ktp_anggota, nama_alamat_anggota) REFERENCES public.alamat(no_ktp_anggota, nama) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

