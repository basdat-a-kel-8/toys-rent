from time import gmtime, strftime
import datetime

from django.shortcuts import render
from .forms import PesananForm, PesananFormAnggota, UpdateAdmin, UpdateAnggota
from user.models import *
from django.db import connection
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
import random
import string
# Create your views here.

def generate_harga():
    tail = random.randint(10000,999999)
    return tail

def generate_id_pemesanan():
    tail = str(random.randint(1000000000,9999999999))
    return tail

def generate_no_urut():
    tail = str(random.randint(120,999))
    return tail

def addPesanan(request):
    if request.method == 'POST':
        print("masuk post")
        no_ktp_anggota = request.session['no_ktp']
        if request.session['role'] == 'admin':
            form = PesananForm(request.POST)
            if form.is_valid():
                no_ktp_anggota = form.cleaned_data.get('anggota')
                print(no_ktp_anggota)
        else:
            form = PesananFormAnggota(request.POST)
        if form.is_valid():
            print("form valid")
            with connection.cursor() as cursor:
                id_pemesanan = generate_id_pemesanan()
                id_pemesanan_exists = list(Pemesanan.objects.raw("select * from pemesanan where id_pemesanan = %s", [id_pemesanan]))
                while id_pemesanan_exists:
                    id_pemesanan = generate_id_pemesanan()
                    id_pemesanan_exists = list(Pemesanan.objects.raw("select * from pemesanan where id_pemesanan = %s", [id_pemesanan])) 
                print(id_pemesanan)
                no_urut = generate_no_urut()
                print(no_urut)
                datetime_pemesanan = strftime('%Y-%m-%d', gmtime())
                print(datetime_pemesanan)
                kuantitas_barang = len(form.cleaned_data.get('barang'))
                harga = generate_harga()
                barang = form.cleaned_data.get('barang')
                print(barang)
                id_barang = list(Barang.objects.raw("select * from barang where nama_item = %s limit 1", barang))
                print(id_barang)
                data = [{
                    'id_barang': i.id_barang,
                } for i in id_barang]
                akhirnyadapetid = (data[0]['id_barang'])
                lama_sewa = form.cleaned_data.get('lamasewa')
                status = 'SEDANG DIKONFIRMASI'
                if request.session['role'] == 'admin':
                    no_ktp_anggota = form.cleaned_data.get('anggota')
                    no_ktp_anggota = no_ktp_anggota[0]
                else:
                    no_ktp_anggota = request.session['no_ktp']
                data_pemesanan = [id_pemesanan, datetime_pemesanan, kuantitas_barang, harga, harga, no_ktp_anggota, status]
                data_barang_pesanan = [id_pemesanan, no_urut, akhirnyadapetid, datetime_pemesanan, lama_sewa, datetime_pemesanan, status]
                cursor.execute(
                    'INSERT INTO pemesanan VALUES(%s, %s, %s, %s, %s, %s, %s)', data_pemesanan
                )
                print("masuk pemesanan")
                cursor.execute(
                    'INSERT INTO barang_pesanan VALUES(%s, %s, %s, %s, %s, %s, %s)', data_barang_pesanan
                )
                print("masuk barang pesanan")
                return HttpResponseRedirect('/pesanan/listpesanan/1')
    
    if request.session['role'] == 'admin':
        form = PesananForm()
    else:
        form = PesananFormAnggota()
    return render(request, 'pesanan/addpesanan.html', {'forms': form, 'harga': generate_harga()})

def updatePesanan(request, id_pemesanan):
    with connection.cursor() as cursor:
        cursor.execute('SELECT b.nama_item, bp.status, bp.no_urut, bp.lama_sewa FROM BARANG_PESANAN bp , BARANG b WHERE b.id_barang = bp.id_barang AND id_pemesanan=%s', [id_pemesanan])
        pesanan = cursor.fetchall()
        print(pesanan)
        cursor.execute('SELECT bp.no_urut FROM BARANG_PESANAN bp, BARANG b WHERE b.id_barang = bp.id_barang AND id_pemesanan=%s', [id_pemesanan])
        nourut = cursor.fetchall()
        print(nourut)
    if request.session['role'] == 'admin':
        pesanan = {'barang': pesanan[0][0], 'status': pesanan[0][1], 'lamasewa': pesanan[0][3]}
    else:
        pesanan = {'barang': pesanan[0][0], 'lamasewa': pesanan[0][3]}
    print(pesanan)
    print(pesanan['barang'])
    nourutfinal = nourut[0][0]
    if request.method == 'POST':
        if request.session['role'] == 'admin':
            form = UpdateAdmin(request.POST, initial=pesanan)
        else:
            form = UpdateAnggota(request.POST, initial=pesanan)
        if form.is_valid() and form.has_changed():
            with connection.cursor() as c:
                barangbaru = form.cleaned_data.get('barang')[0]
                id_barang = list(Barang.objects.raw("select * from barang where nama_item = %s limit 1", [barangbaru]))
                print(id_barang)
                data = [{
                    'id_barang': i.id_barang,
                } for i in id_barang]
                akhirnyadapetid = (data[0]['id_barang'])
                if request.session['role'] == 'admin':
                    status = form.cleaned_data.get('status')[0]
                    print(status)
                    c.execute("UPDATE barang_pesanan SET id_barang=%s, lama_sewa=%s, status=%s WHERE no_urut=%s and id_pemesanan=%s", [akhirnyadapetid, form.cleaned_data.get('lamasewa'), status, nourutfinal, id_pemesanan])
                    c.execute("UPDATE pemesanan SET status=%s WHERE id_pemesanan=%s", [status, id_pemesanan])
                else:
                    c.execute("UPDATE barang_pesanan SET id_barang=%s, lama_sewa=%s WHERE no_urut=%s and id_pemesanan=%s", [akhirnyadapetid, form.cleaned_data.get('lamasewa'), nourutfinal, id_pemesanan])
            return HttpResponseRedirect('/pesanan/listpesanan/1')
        else:
            print("dalem else luar")
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        if request.session['role'] == 'admin':
            form = UpdateAdmin(pesanan) 
        else:
            form = UpdateAnggota(pesanan)
        return render(request, 'pesanan/updatepesanan.html', {'forms': form})
    # return render(request, 'pesanan/updatepesanan.html')

def deletePesanan(request, nama):
    with connection.cursor() as c:
        c.execute("DELETE FROM barang_pesanan WHERE no_urut = %s", [nama])
    return HttpResponseRedirect('/pesanan/listpesanan/1')

def listPesanan(request, page):
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT p.id_pemesanan, b.nama_item, p.harga_sewa, p.status, bp.no_urut FROM PEMESANAN p, BARANG_PESANAN bp , BARANG b WHERE bp.id_pemesanan = p.id_pemesanan AND b.id_barang = bp.id_barang'
        )
        pesanan = cursor.fetchall()
    # print(pesanan)
    data = [{'id_pemesanan':i[0],
            'nama_item' : i[1],
            'harga_sewa': i[2],
            'status': i[3],
            'no_urut':i[4]} for i in pesanan]
    # print(data)
    paginator = Paginator(data, 10)
    try:
        dataa = paginator.page(page)
    except PageNotAnInteger:
        dataa = paginator.page(1)
    except EmptyPage:
        dataa = paginator.page(paginator.num_pages)
    return render(request, 'pesanan/daftarpesanan.html', {'pesanan':dataa})
    
def statusFiltering(request, kategori, page):
    with connection.cursor() as cursor:
        cursor.execute(
            'SELECT p.id_pemesanan, b.nama_item, p.harga_sewa, p.status, bp.no_urut FROM PEMESANAN p, BARANG_PESANAN bp , BARANG b WHERE bp.id_pemesanan = p.id_pemesanan AND b.id_barang = bp.id_barang AND p.status=%s',[kategori]
        )
        pesanan = cursor.fetchall()
    print(pesanan)
    data = [{'id_pemesanan':i[0],
            'nama_item' : i[1],
            'harga_sewa': i[2],
            'status': i[3],
            'no_urut':i[4]} for i in pesanan]
    paginator = Paginator(data, 10)
    try:
        dataa = paginator.page(page)
    except PageNotAnInteger:
        dataa = paginator.page(1)
    except EmptyPage:
        dataa = paginator.page(paginator.num_pages)
    return render(request, 'pesanan/daftarpesanan.html', {'pesanan':dataa})