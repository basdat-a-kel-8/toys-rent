from django import forms
from django.core.exceptions import ValidationError
from django.db import connection

lamasewa_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Lama Sewa (hari)',
}

def anggota_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT a.no_ktp from anggota a, pengguna p where p.no_ktp = a.no_ktp")
        sini = cursor.fetchall()
        result = [(i[0], i[0]) for i in sini]
        return result

def item_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama_item from barang")
        sini = cursor.fetchall()
        result = [(i[0], i[0]) for i in sini]
        return result

def status_choices():
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama from status")
        sini = cursor.fetchall()
        result = [(i[0], i[0]) for i in sini]
        return result

pemilikpenyewa_attrs = {
    'class': 'form-control form-border-pink',
}


class PesananForm(forms.Form):
    barang = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=item_choices())
    lamasewa = forms.CharField(max_length=255, widget=forms.TextInput(attrs=lamasewa_attrs))
    anggota = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=anggota_choices())

class PesananFormAnggota(forms.Form):
    barang = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=item_choices())
    lamasewa = forms.CharField(max_length=255, widget=forms.TextInput(attrs=lamasewa_attrs))

class UpdateAdmin(forms.Form):
    barang = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=item_choices())
    lamasewa = forms.CharField(max_length=255, widget=forms.TextInput(attrs=lamasewa_attrs))
    status = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=status_choices())

class UpdateAnggota(forms.Form):
    barang = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs=pemilikpenyewa_attrs), choices=item_choices())
    lamasewa = forms.CharField(max_length=255, widget=forms.TextInput(attrs=lamasewa_attrs))