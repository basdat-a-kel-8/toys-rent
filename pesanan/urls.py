from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'pesanan'
urlpatterns = [
    path('addpesanan', addPesanan, name='addPesanan'),
    path('updatepesanan/<str:id_pemesanan>', updatePesanan, name='updatePesanan'),
    path('listpesanan/<str:page>', listPesanan, name='listPesanan'),
    path('listpesanan/<str:kategori>/<str:page>', statusFiltering, name="filtering"),
    path('deletepesanan/<str:nama>', deletePesanan, name='delete')
]