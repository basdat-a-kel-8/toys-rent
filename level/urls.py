from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'level'
urlpatterns = [
    path('addlevel', addLevel, name='addLevel'),
    path('updatelevel/<str:nama_level>', updateLevel, name="updateLevel"),
    path('listlevel', listLevel, name='listLevel'),
    path('deletelevel/<str:nama_level>', deleteLevel, name='delete'),
]