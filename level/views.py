from django.shortcuts import render
from .forms import LevelForm
from user.models import *
from django.db import connection
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.
def addLevel(request):
    # forms = LevelForm()
    # return render (request, 'level/addlevel.html', {'forms':forms})
    if request.method == "POST":
        form = LevelForm(request.POST)
        if form.is_valid():
            data_level = [form.cleaned_data.get('namalevel'), form.cleaned_data.get('makspoin'), form.cleaned_data.get('deskripsi')]
            with connection.cursor() as c:
                c.execute("INSERT INTO level_keanggotaan VALUES(%s, %s, %s)", data_level)
                return HttpResponseRedirect('listlevel')
        else:
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        forms = LevelForm()
        return render (request, 'level/addlevel.html', {'forms':forms})

def updateLevel(request):
    return render(request, 'level/updatelevel.html')

# def listLevel(request):
#     return render(request, 'level/daftarlevel.html')

def listLevel(request):
    level = list(LevelKeanggotaan.objects.raw('SELECT * FROM level_keanggotaan'))
    print(level)
    data = [{
        'nama_level': i.nama_level,
        'minimum_poin': i.minimum_poin,
        'deskripsi': i.deskripsi,

    } for i in level]
    return render(request, 'level/daftarlevel.html', {'level': data})

def deleteLevel(request, nama_level):
    print(nama_level)
    with connection.cursor() as cursor:
        cursor.execute(
            'DELETE FROM level_keanggotaan where nama_level = %s', [nama_level]
        )
    return HttpResponseRedirect('/level/listlevel')

def updateLevel(request, nama_level):
    with connection.cursor() as c:
        c.execute("select * from level_keanggotaan where nama_level = %s", [nama_level])
        level_query = c.fetchall()
    level = {'namalevel': level_query[0][0], 'makspoin': level_query[0][1], 'deskripsi': level_query[0][2]}
    if request.method == 'POST':
        form = LevelForm(request.POST, initial=level)
        if form.is_valid() and form.has_changed():
            if 'namalevel' in form.changed_data:
                print("namalevelberubah")
                with connection.cursor() as c:
                    createLevel(form)
            else:
                print("namalevel ga berubah")
                with connection.cursor() as c:
                    createLevel(form)
            return HttpResponseRedirect('/level/listlevel')
        else:
            print("dalem else luar")
            print(form.errors)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        form = LevelForm(level) 
        return render(request, 'level/updatelevel.html', {'forms': form, 'nama_level': nama_level})

def createLevel(form):
    data_level = [form.cleaned_data.get('namalevel'), form.cleaned_data.get('makspoin'), form.cleaned_data.get('deskripsi')]
    with connection.cursor() as c:
        c.execute("DELETE FROM level_keanggotaan WHERE nama_level = %s", [form.cleaned_data.get('namalevel')])
        c.execute("INSERT INTO level_keanggotaan VALUES(%s, %s, %s)", data_level)