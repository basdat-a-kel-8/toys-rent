from django import forms
from django.core.exceptions import ValidationError

namalevel_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Nama Level',
}

makspoin_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Minimal Poin',
}

deskripsi_attrs = {
    'type': 'text',
    'class': 'form-control form-border-pink',
    'placeholder':'Deskripsi',
}

class LevelForm(forms.Form):
    namalevel = forms.CharField(max_length=255, widget=forms.TextInput(attrs=namalevel_attrs))
    deskripsi = forms.CharField(max_length=1000, widget=forms.TextInput(attrs=deskripsi_attrs))
    makspoin = forms.IntegerField(widget=forms.NumberInput(attrs=makspoin_attrs))